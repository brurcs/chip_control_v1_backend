FROM openjdk:11.0.2-jdk-oraclelinux7

# Defaults environments
ENV DB=DSOO395:3306/chip_control_v2
ENV DB_USER=root
ENV DB_PASSWORD=root
ENV CHIP_TOKEN=e4ab3e9dcb695704a4e8be09a9db7b01ec6beac9
ENV SIGMA_URL=https://apps.orsegups.com.br:8080/sigma-service
ENV SIGMA_TOKEN=9UcfUN8aiGcH60WlYK5L6CdzASdOCwbZQEehYZFgl48mXekf8F0NzANya8Co15VK

# Prepare app directory
VOLUME /tmp
RUN mkdir -p /usr/src/app
COPY . /usr/src/app
WORKDIR /usr/src/app

RUN ./mvnw clean package -Pdocker
RUN cp target/chip-control-v2-backend*.war /app.jar
RUN sh -c 'touch /app.jar'

EXPOSE 8080
ENV JAVA_OPTS="-Dspring.profiles.active=docker"
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /app.jar" ]

