package br.com.orsegups.dto;

import br.com.orsegups.entity.EquipmentLink;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class EquipmentLinkDTO {

    private Long id;
    private ChipDTO chip;
    private String identifier;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date installationDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date removalDate;
    private String removalReason;
    private RegionalOfficeDTO regionalOffice;


    public EquipmentLinkDTO() {
        super();
    }

    public EquipmentLinkDTO(Long id, String identifier, Date installationDate, Date removalDate,
                            String removalReason, RegionalOfficeDTO regionalOffice) {
        this.id = id;
        this.identifier = identifier;
        this.installationDate = installationDate;
        this.removalDate = removalDate;
        this.removalReason = removalReason;
        this.regionalOffice = regionalOffice;
    }

    public EquipmentLinkDTO(EquipmentLink equipmentLink, Boolean includeChip) {
        this(
            equipmentLink.getId(),
            equipmentLink.getIdentifier(),
            equipmentLink.getInstallationDate(),
            equipmentLink.getRemovalDate(),
            equipmentLink.getRemovalReason(),
            new RegionalOfficeDTO(equipmentLink.getRegionalOffice())
        );

        if (includeChip) {
            this.chip = new ChipDTO(equipmentLink.getChip());
        }
    }

    public EquipmentLinkDTO(EquipmentLink equipmentLink) {
        this(equipmentLink, true);
    }

    public Long getId() {
        return id;
    }

    public ChipDTO getChip() {
        return chip;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public Date getRemovalDate() {
        return removalDate;
    }

    public String getRemovalReason() {
        return removalReason;
    }

    public RegionalOfficeDTO getRegionalOffice() {
        return regionalOffice;
    }
}
