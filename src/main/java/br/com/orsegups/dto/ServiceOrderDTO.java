package br.com.orsegups.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ServiceOrderDTO {

    private Long id;
    private Long companyId;
    private String centralId;
    private String centralPartition;
    private String businessName;
    private Boolean customerActive;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date centralCreatedDate;

    public ServiceOrderDTO() {
        super();
    }

    public ServiceOrderDTO(Long id, Long companyId, String centralId, String centralPartition, String businessName, Boolean customerActive, Date centralCreatedDate) {
        this.id = id;
        this.companyId = companyId;
        this.centralId = centralId;
        this.centralPartition = centralPartition;
        this.businessName = businessName;
        this.customerActive = customerActive;
        this.centralCreatedDate = centralCreatedDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCentralId() {
        return centralId;
    }

    public void setCentralId(String centralId) {
        this.centralId = centralId;
    }

    public String getCentralPartition() {
        return centralPartition;
    }

    public void setCentralPartition(String centralPartition) {
        this.centralPartition = centralPartition;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Boolean getCustomerActive() {
        return customerActive;
    }

    public void setCustomerActive(Boolean customerActive) {
        this.customerActive = customerActive;
    }

    public Date getCentralCreatedDate() {
        return centralCreatedDate;
    }

    public void setCentralCreatedDate(Date centralCreatedDate) {
        this.centralCreatedDate = centralCreatedDate;
    }
}
