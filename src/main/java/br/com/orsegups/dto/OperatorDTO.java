package br.com.orsegups.dto;

import br.com.orsegups.entity.Operator;

public class OperatorDTO {

    private Long id;
    private String name;

    public OperatorDTO() {
        super();
    }

    public OperatorDTO(Long id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public OperatorDTO(Operator operator) {
        this(
            operator.getId(),
            operator.getName()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}