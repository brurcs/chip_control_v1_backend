package br.com.orsegups.dto;

import br.com.orsegups.entity.TechnicianDelivery;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TechnicianDeliveryDTO {

    private Long id;
    private Long technicianId;
    private String technicianName;
    private RegionalOfficeDTO regionalOffice;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date deliveryDate;
    private List<TechnicianDeliveryChipDTO> technicianDeliveryChip;

    public TechnicianDeliveryDTO() {
        super();
    }

    public TechnicianDeliveryDTO(Long id, Long technicianId, String technicianName, RegionalOfficeDTO regionalOffice, Date deliveryDate) {
        this();
        this.id = id;
        this.technicianId = technicianId;
        this.technicianName = technicianName;
        this.regionalOffice = regionalOffice;
        this.deliveryDate = deliveryDate;
    }

    public TechnicianDeliveryDTO(TechnicianDelivery technicianDelivery, Boolean includeChips) {
        this(
            technicianDelivery.getId(),
            technicianDelivery.getTechnicianId(),
            technicianDelivery.getTechnicianName(),
            new RegionalOfficeDTO(technicianDelivery.getRegionalOffice()),
            technicianDelivery.getDeliveryDate()
        );

        if (includeChips) {
            this.technicianDeliveryChip = technicianDelivery.getTechnicianDeliveryChips().stream()
                    .map(TechnicianDeliveryChipDTO::new).collect(Collectors.toList());
        }
    }

    public TechnicianDeliveryDTO(TechnicianDelivery technicianDelivery) {
        this(technicianDelivery, true);
    }

    public Long getId() {
        return id;
    }

    public Long getTechnicianId() {
        return technicianId;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public RegionalOfficeDTO getRegionalOffice() {
        return regionalOffice;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public List<TechnicianDeliveryChipDTO> getTechnicianDeliveryChip() {
        return technicianDeliveryChip;
    }
}
