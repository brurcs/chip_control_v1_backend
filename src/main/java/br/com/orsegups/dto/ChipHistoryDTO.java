package br.com.orsegups.dto;

import br.com.orsegups.entity.ChipHistory;
import br.com.orsegups.entity.type.HistoryType;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChipHistoryDTO {

    private Long id;
    private ChipDTO chip;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date historyDate;
    private HistoryType historyType;
    private String observation;
    private String user;
    private RegionalOfficeDTO regionalOffice;
    private ChipLotDTO chipLot;
    private TechnicianDeliveryDTO technicianDelivery;
    private CustomerLinkDTO customerLink;
    private EquipmentLinkDTO equipmentLink;


    public ChipHistoryDTO() {
        super();
    }

    public ChipHistoryDTO(Long id, ChipDTO chip, Date historyDate, HistoryType historyType, String observation, String user) {
        this();
        this.id = id;
        this.chip = chip;
        this.historyDate = historyDate;
        this.historyType = historyType;
        this.observation = observation;
        this.user = user;
    }

    public ChipHistoryDTO(ChipHistory chipHistory) {
        this(
            chipHistory.getId(),
            new ChipDTO(chipHistory.getChip()),
            chipHistory.getHistoryDate(),
            chipHistory.getHistoryType(),
            chipHistory.getObservation(),
            chipHistory.getUser()
        );

        if (chipHistory.getRegionalOffice() != null)
            this.regionalOffice = new RegionalOfficeDTO(chipHistory.getRegionalOffice());
        if (chipHistory.getChipLot() != null)
            this.chipLot = new ChipLotDTO(chipHistory.getChipLot(), false);
        if (chipHistory.getTechnicianDelivery() != null)
            this.technicianDelivery = new TechnicianDeliveryDTO(chipHistory.getTechnicianDelivery(), false);
        if (chipHistory.getCustomerLink() != null)
            this.customerLink = new CustomerLinkDTO(chipHistory.getCustomerLink(), false);
        if (chipHistory.getEquipmentLink() != null)
            this.equipmentLink = new EquipmentLinkDTO(chipHistory.getEquipmentLink(), false);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChipDTO getChip() {
        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }

    public Date getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(Date historyDate) {
        this.historyDate = historyDate;
    }

    public HistoryType getHistoryType() {
        return historyType;
    }

    public void setHistoryType(HistoryType historyType) {
        this.historyType = historyType;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public RegionalOfficeDTO getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(RegionalOfficeDTO regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public ChipLotDTO getChipLot() {
        return chipLot;
    }

    public void setChipLot(ChipLotDTO chipLot) {
        this.chipLot = chipLot;
    }

    public TechnicianDeliveryDTO getTechnicianDelivery() {
        return technicianDelivery;
    }

    public void setTechnicianDelivery(TechnicianDeliveryDTO technicianDelivery) {
        this.technicianDelivery = technicianDelivery;
    }

    public CustomerLinkDTO getCustomerLink() {
        return customerLink;
    }

    public void setCustomerLink(CustomerLinkDTO customerLink) {
        this.customerLink = customerLink;
    }

    public EquipmentLinkDTO getEquipmentLink() {
        return equipmentLink;
    }

    public void setEquipmentLink(EquipmentLinkDTO equipmentLink) {
        this.equipmentLink = equipmentLink;
    }
}