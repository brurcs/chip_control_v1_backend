package br.com.orsegups.dto;

import br.com.orsegups.entity.CustomerLink;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CustomerLinkDTO {

    private Long id;
    private Long technicianId;
    private String technicianName;
    private Long orderId;
    private Long companyId;
    private String centralId;
    private String partition;
    private String businessName;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date installationDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date removalDate;
    private String removalReason;
    private ChipDTO chip;

    public CustomerLinkDTO() {
        super();
    }

    public CustomerLinkDTO(Long id, Long technicianId, String technicianName, Long orderId, Long companyId, String centralId,
                           String partition, String businessName, Date installationDate, Date removalDate, String removalReason) {
        this();
        this.id = id;
        this.technicianId = technicianId;
        this.technicianName = technicianName;
        this.orderId = orderId;
        this.companyId = companyId;
        this.centralId = centralId;
        this.partition = partition;
        this.businessName = businessName;
        this.installationDate = installationDate;
        this.removalDate = removalDate;
        this.removalReason = removalReason;
    }

    public CustomerLinkDTO(CustomerLink customerLink, Boolean includeChip) {
        this(
            customerLink.getId(),
            customerLink.getTechnicianId(),
            customerLink.getTechnicianName(),
            customerLink.getOrderId(),
            customerLink.getCompanyId(),
            customerLink.getCentralId(),
            customerLink.getPartition(),
            customerLink.getBusinessName(),
            customerLink.getInstallationDate(),
            customerLink.getRemovalDate(),
            customerLink.getRemovalReason()
        );

        if (includeChip) {
            this.chip = new ChipDTO(customerLink.getChip());
        }
    }

    public CustomerLinkDTO(CustomerLink customerLink) {
        this(customerLink, true);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Long technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCentralId() {
        return centralId;
    }

    public void setCentralId(String centralId) {
        this.centralId = centralId;
    }

    public String getPartition() {
        return partition;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public Date getRemovalDate() {
        return removalDate;
    }

    public void setRemovalDate(Date removalDate) {
        this.removalDate = removalDate;
    }

    public String getRemovalReason() {
        return removalReason;
    }

    public void setRemovalReason(String removalReason) {
        this.removalReason = removalReason;
    }

    public ChipDTO getChip() {
        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }
}
