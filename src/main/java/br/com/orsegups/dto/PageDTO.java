package br.com.orsegups.dto;

import java.util.List;

public class PageDTO<T> {

    private Integer totalPages;
    private Long totalItems;
    private List<T> items;

    public PageDTO(Integer totalPages, Long totalItems, List<T> items) {
        this.totalPages = totalPages;
        this.totalItems = totalItems;
        this.items = items;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
