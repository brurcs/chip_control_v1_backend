package br.com.orsegups.dto;

import br.com.orsegups.entity.ChipLotItem;
import br.com.orsegups.entity.type.ChipLotItemStatusType;

public class ChipLotItemDTO {

    private Long id;
    private ChipDTO chip;
    private ChipLotItemStatusType status;

    public ChipLotItemDTO() {
        super();
    }

    public ChipLotItemDTO(Long id, ChipDTO chip, ChipLotItemStatusType status) {
        this();
        this.id = id;
        this.chip = chip;
        this.status = status;
    }

    public ChipLotItemDTO(ChipLotItem chipLotItem) {
        this(
            chipLotItem.getId(),
            new ChipDTO(chipLotItem.getChip()),
            chipLotItem.getStatus()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChipDTO getChip() {
        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }

    public ChipLotItemStatusType getStatus() {
        return status;
    }

    public void setStatus(ChipLotItemStatusType status) {
        this.status = status;
    }
}
