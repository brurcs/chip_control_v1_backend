package br.com.orsegups.dto;

import java.util.List;

public class CustomerToCancelDTO {

    private String cutoffDate;
    private List<CustomerDTO> customers;

    public CustomerToCancelDTO() {
        super();
    }

    public CustomerToCancelDTO(String cutoffDate, List<CustomerDTO> customers) {
        this();
        this.cutoffDate = cutoffDate;
        this.customers = customers;
    }

    public String getCutoffDate() {
        return cutoffDate;
    }

    public void setCutoffDate(String cutoffDate) {
        this.cutoffDate = cutoffDate;
    }

    public List<CustomerDTO> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerDTO> customers) {
        this.customers = customers;
    }
}
