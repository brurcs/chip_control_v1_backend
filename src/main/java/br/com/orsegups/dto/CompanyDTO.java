package br.com.orsegups.dto;

import br.com.orsegups.entity.Company;

public class CompanyDTO {

    private Long id;
    private String name;

    public CompanyDTO() {
        super();
    }

    public CompanyDTO(Long id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public CompanyDTO(Company company) {
        this(company.getId(), company.getName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}