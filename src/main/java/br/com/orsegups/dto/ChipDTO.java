package br.com.orsegups.dto;

import br.com.orsegups.entity.Chip;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ChipDTO {

    private Long id;
    private String iccId;
    private String phoneNumber;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date createdDate;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date requestedCancellationDate;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date cancellationDate;
    private String cancellationReason;
    private RegionalOfficeDTO canceledFromRegional;
    private Boolean toRemove;
    private CompanyDTO company;
    private OperatorDTO operator;
    private RegionalOfficeDTO regionalOffice;
    private TechnicianDeliveryDTO technicianDelivery;
    private CustomerLinkDTO customerLink;
    private EquipmentLinkDTO equipmentLink;

    public ChipDTO() {
        super();
    }

    public ChipDTO(Long id, String iccId, String phoneNumber, Date createdDate, Date requestedCancellationDate, Date cancellationDate,
                   String cancellationReason, Boolean toRemove, CompanyDTO company, OperatorDTO operator) {
        this();
        this.id = id;
        this.iccId = iccId;
        this.phoneNumber = phoneNumber;
        this.createdDate = createdDate;
        this.requestedCancellationDate = requestedCancellationDate;
        this.cancellationDate = cancellationDate;
        this.cancellationReason = cancellationReason;
        this.toRemove = toRemove;
        this.company = company;
        this.operator = operator;
    }

    public ChipDTO(Chip chip) {
        this(
            chip.getId(),
            chip.getIccId(),
            chip.getPhoneNumber(),
            chip.getCreatedDate(),
            chip.getRequestedCancellationDate(),
            chip.getCancellationDate(),
            chip.getCancellationReason(),
            chip.getToRemove(),
            new CompanyDTO(chip.getCompany()),
            new OperatorDTO(chip.getOperator())
        );

        if (chip.getCanceledFromRegional() != null)
            this.canceledFromRegional = new RegionalOfficeDTO(chip.getCanceledFromRegional());
        if (chip.getRegionalOffice() != null)
            this.regionalOffice = new RegionalOfficeDTO(chip.getRegionalOffice());
        if (chip.getTechnicianDelivery() != null)
            this.technicianDelivery = new TechnicianDeliveryDTO(chip.getTechnicianDelivery(), false);
        if (chip.getCustomerLink() != null)
            this.customerLink = new CustomerLinkDTO(chip.getCustomerLink(), false);
        if (chip.getEquipmentLink() != null)
            this.equipmentLink = new EquipmentLinkDTO(chip.getEquipmentLink(), false);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getRequestedCancellationDate() {
        return requestedCancellationDate;
    }

    public void setRequestedCancellationDate(Date requestedCancellationDate) {
        this.requestedCancellationDate = requestedCancellationDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public RegionalOfficeDTO getCanceledFromRegional() {
        return canceledFromRegional;
    }

    public void setCanceledFromRegional(RegionalOfficeDTO canceledFromRegional) {
        this.canceledFromRegional = canceledFromRegional;
    }

    public Boolean getToRemove() {
        return toRemove;
    }

    public void setToRemove(Boolean toRemove) {
        this.toRemove = toRemove;
    }

    public CompanyDTO getCompany() {
        return company;
    }

    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public OperatorDTO getOperator() {
        return operator;
    }

    public void setOperator(OperatorDTO operator) {
        this.operator = operator;
    }

    public RegionalOfficeDTO getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(RegionalOfficeDTO regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public TechnicianDeliveryDTO getTechnicianDelivery() {
        return technicianDelivery;
    }

    public void setTechnicianDelivery(TechnicianDeliveryDTO technicianDelivery) {
        this.technicianDelivery = technicianDelivery;
    }

    public CustomerLinkDTO getCustomerLink() {
        return customerLink;
    }

    public void setCustomerLink(CustomerLinkDTO customerLink) {
        this.customerLink = customerLink;
    }

    public EquipmentLinkDTO getEquipmentLink() {
        return equipmentLink;
    }

    public void setEquipmentLink(EquipmentLinkDTO equipmentLink) {
        this.equipmentLink = equipmentLink;
    }
}