package br.com.orsegups.dto;

import org.springframework.data.domain.Page;

public class TableOptionDTO {

    private Integer currentPage;
    private Integer itemPerPage;
    private Long totalItems;
    private Integer totalPages;

    public TableOptionDTO() {
        this.currentPage = 1;
        this.itemPerPage = 1;
        this.totalItems = 0L;
        this.totalPages = 0;
    }

    public TableOptionDTO(Integer currentPage, Integer itemPerPage, Long totalItems, Integer totalPages) {
        this.currentPage = currentPage;
        this.itemPerPage = itemPerPage;
        this.totalItems = totalItems;
        this.totalPages = totalPages;
    }

    public TableOptionDTO(PageDTO page, TableOptionDTO tableOptionDTO) {
        this(
            tableOptionDTO.getCurrentPage() > page.getTotalPages() ? page.getTotalPages() : tableOptionDTO.getCurrentPage(),
            tableOptionDTO.getItemPerPage(),
            page.getTotalItems(),
            page.getTotalPages()
        );
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getItemPerPage() {
        return itemPerPage;
    }

    public void setItemPerPage(Integer itemPerPage) {
        this.itemPerPage = itemPerPage;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public void setPage(Page page) {
        this.setTotalItems(page.getTotalElements());
        this.setTotalPages(page.getTotalPages());
    }

    public void setPageDTO(PageDTO page) {
        this.setTotalItems(page.getTotalItems());
        this.setTotalPages(page.getTotalPages());
    }
}
