package br.com.orsegups.dto;

import br.com.orsegups.entity.RegionalOffice;

public class RegionalOfficeDTO {

    private Long id;
    private String name;
    private String acronym;
    private Integer standardStock;

    public RegionalOfficeDTO() {
        super();
    }

    public RegionalOfficeDTO(Long id, String name, String acronym, Integer standardStock) {
        this();
        this.id = id;
        this.name = name;
        this.acronym = acronym;
        this.standardStock = standardStock;
    }

    public RegionalOfficeDTO(RegionalOffice regionalOffice) {
        this(
            regionalOffice.getId(),
            regionalOffice.getName(),
            regionalOffice.getAcronym(),
            regionalOffice.getStandardStock()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Integer getStandardStock() {
        return standardStock;
    }

    public void setStandardStock(Integer standardStock) {
        this.standardStock = standardStock;
    }
}