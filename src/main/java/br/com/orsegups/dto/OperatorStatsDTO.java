package br.com.orsegups.dto;

public class OperatorStatsDTO {

    private OperatorDTO operator;
    private Integer activeAmount;
    private Integer cancellationAmount;

    public OperatorStatsDTO() {
        super();
    }

    public OperatorStatsDTO(OperatorDTO operator) {
        this();
        this.operator = operator;
    }

    public OperatorDTO getOperator() {
        return operator;
    }

    public void setOperator(OperatorDTO operator) {
        this.operator = operator;
    }

    public Integer getActiveAmount() {
        return activeAmount;
    }

    public void setActiveAmount(Integer activeAmount) {
        this.activeAmount = activeAmount;
    }

    public Integer getCancellationAmount() {
        return cancellationAmount;
    }

    public void setCancellationAmount(Integer cancellationAmount) {
        this.cancellationAmount = cancellationAmount;
    }
}
