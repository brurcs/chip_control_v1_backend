package br.com.orsegups.dto;

import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.type.ChipLotStatusType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ChipLotDTO {

    private Long id;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date createdDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date sendDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date expectedArrivalDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date arrivalDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date cancellationDate;
    private RegionalOfficeDTO regionalOrigin;
    private RegionalOfficeDTO regionalDestination;
    private List<ChipLotItemDTO> chipLotItems;
    private Integer sentAmount;
    private Integer receivedAmount;
    private ChipLotStatusType status;

    public ChipLotDTO() {
        super();
    }

    public ChipLotDTO(Long id, Date createdDate, Date sendDate, Date expectedArrivalDate, Date arrivalDate, Date cancellationDate, RegionalOfficeDTO regionalOrigin,
                      RegionalOfficeDTO regionalDestination, Integer sentAmount, Integer receivedAmount, ChipLotStatusType status) {
        this();
        this.id = id;
        this.createdDate = createdDate;
        this.sendDate = sendDate;
        this.expectedArrivalDate = expectedArrivalDate;
        this.arrivalDate = arrivalDate;
        this.cancellationDate = cancellationDate;
        this.regionalOrigin = regionalOrigin;
        this.regionalDestination = regionalDestination;
        this.sentAmount = sentAmount;
        this.receivedAmount = receivedAmount;
        this.status = status;
    }

    public ChipLotDTO(ChipLot chipLot, Boolean includeChips) {
        this(
            chipLot.getId(),
            chipLot.getCreatedDate(),
            chipLot.getSendDate(),
            chipLot.getExpectedArrivalDate(),
            chipLot.getArrivalDate(),
            chipLot.getCancellationDate(),
            new RegionalOfficeDTO(chipLot.getRegionalOrigin()),
            new RegionalOfficeDTO(chipLot.getRegionalDestination()),
            chipLot.getSentAmount(),
            chipLot.getReceivedAmount(),
            chipLot.getStatus()
        );

        if (includeChips) {
            this.chipLotItems = chipLot.getChipLotItems().stream().map(ChipLotItemDTO::new).collect(Collectors.toList());
        }
    }

    public ChipLotDTO(ChipLot chipLot) {
        this(chipLot, true);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(Date expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public void setCancellationDate(Date cancellationDate) {
        this.cancellationDate = cancellationDate;
    }

    public RegionalOfficeDTO getRegionalOrigin() {
        return regionalOrigin;
    }

    public void setRegionalOrigin(RegionalOfficeDTO regionalOrigin) {
        this.regionalOrigin = regionalOrigin;
    }

    public RegionalOfficeDTO getRegionalDestination() {
        return regionalDestination;
    }

    public void setRegionalDestination(RegionalOfficeDTO regionalDestination) {
        this.regionalDestination = regionalDestination;
    }

    public List<ChipLotItemDTO> getChipLotItems() {
        return chipLotItems;
    }

    public void setChipLotItems(List<ChipLotItemDTO> chipLotItems) {
        this.chipLotItems = chipLotItems;
    }

    public Integer getSentAmount() {
        return sentAmount;
    }

    public void setSentAmount(Integer sentAmount) {
        this.sentAmount = sentAmount;
    }

    public Integer getReceivedAmount() {
        return receivedAmount;
    }

    public void setReceivedAmount(Integer receivedAmount) {
        this.receivedAmount = receivedAmount;
    }

    public ChipLotStatusType getStatus() {
        return status;
    }

    public void setStatus(ChipLotStatusType status) {
        this.status = status;
    }
}
