package br.com.orsegups.dto;

import br.com.orsegups.entity.TechnicianDeliveryChip;
import br.com.orsegups.entity.type.TechnicianDeliveryStatusType;

import javax.persistence.Entity;

public class TechnicianDeliveryChipDTO {

    private Long id;
    private ChipDTO chip;
    private TechnicianDeliveryStatusType status;

    public TechnicianDeliveryChipDTO() {
        super();
    }

    public TechnicianDeliveryChipDTO(Long id, ChipDTO chip, TechnicianDeliveryStatusType status) {
        this();
        this.id = id;
        this.chip = chip;
        this.status = status;
    }

    public TechnicianDeliveryChipDTO(TechnicianDeliveryChip technicianDeliveryChip) {
        this(
            technicianDeliveryChip.getId(),
            new ChipDTO(technicianDeliveryChip.getChip()),
            technicianDeliveryChip.getStatus()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChipDTO getChip() {
        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }

    public TechnicianDeliveryStatusType getStatus() {
        return status;
    }

    public void setStatus(TechnicianDeliveryStatusType status) {
        this.status = status;
    }
}
