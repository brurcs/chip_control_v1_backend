package br.com.orsegups.externalService;

public class SigmaResponseWrapper<T> {
    private Long status;
    private String msg;
    private T ret;

    public SigmaResponseWrapper() {
        super();
    }

    public SigmaResponseWrapper(Long status, String msg, T ret) {
        super();
        this.status = status;
        this.msg = msg;
        this.ret = ret;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getRet() {
        return ret;
    }

    public void setRet(T ret) {
        this.ret = ret;
    }

}
