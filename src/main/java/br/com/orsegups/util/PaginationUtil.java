package br.com.orsegups.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

public class PaginationUtil {

    public static Integer getTotalOfPages(Long totalItems, int pageSize) {
        if (totalItems == 0) return 1;
        return Long.valueOf(totalItems % pageSize == 0 ? totalItems / pageSize : totalItems / pageSize + 1).intValue();
    }

    public static Pageable checkMaxPage(Integer totalPages, Pageable pageable) {
        if (pageable.getPageNumber() > (totalPages - 1)) {
            return PageRequest.of(totalPages - 1, pageable.getPageSize(), pageable.getSort());
        }
        return pageable;
    }

}
