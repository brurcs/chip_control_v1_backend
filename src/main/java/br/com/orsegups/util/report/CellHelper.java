package br.com.orsegups.util.report;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Date;

public class CellHelper {

    public static final String FORMAT_TEXT = "text";
    public static final String FORMAT_DATE = "date";

    public static CellStyle getDateStyle(Workbook workbook) {
        return getStyleFormat(workbook, "dd/MM/yyyy hh:mm:ss");
    }

    public static CellStyle getTextStyle(Workbook workbook) {
        return getStyleFormat(workbook, "@");
    }

    private static CellStyle getStyleFormat(Workbook workbook, String format) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setDataFormat(workbook.getCreationHelper().createDataFormat().getFormat(format));
        return cellStyle;
    }

    public static void setCellValue(Cell cell, Object value) {
        if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Date) {
            cell.setCellValue((Date) value);
        }
    }

}
