package br.com.orsegups.util.report;

import java.io.ByteArrayInputStream;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.collections4.ListUtils;
import org.springframework.stereotype.Component;

import br.com.orsegups.dto.ChipDTO;

@Component
public class ReportTransformer {
	List<ReportHeader> defaultHeader = Arrays.asList(
			new ReportHeader(0, "IccId", CellHelper.FORMAT_TEXT),
			new ReportHeader(1, "Operadora", CellHelper.FORMAT_TEXT),
			new ReportHeader(2, "Empresa", CellHelper.FORMAT_TEXT),
			new ReportHeader(3, "Regional", CellHelper.FORMAT_TEXT)
	);

	public ByteArrayInputStream chipInCancellationToXls(List<ChipDTO> chips) throws Exception {
		List<ReportHeader> header = Arrays.asList(
				new ReportHeader(4, "Solicitado", CellHelper.FORMAT_DATE),
				new ReportHeader(5, "Motivo", CellHelper.FORMAT_TEXT)
		);

		ReportModel model = new ReportModel();
		model.setHeader(ListUtils.union(defaultHeader, header));
		model.setRows(chips.stream().map(chip -> {
			List<Object> cells = new ArrayList<>();
			cells.add(chip.getIccId());
			cells.add(chip.getOperator().getName());
			cells.add(chip.getCompany().getName());
			cells.add(chip.getCanceledFromRegional() != null ? chip.getCanceledFromRegional().getName() : "");
			cells.add(chip.getRequestedCancellationDate() != null ? chip.getRequestedCancellationDate() : "");
			cells.add(chip.getCancellationReason());
			return cells;
		}).collect(Collectors.toList()));

		return ReportFactory.createExcel(model);
	}

	private ReportModel chipInTechnicianQueryToModel(List<ChipDTO> chips) {
		List<ReportHeader> header = Arrays.asList(
				new ReportHeader(4, "Entregue", CellHelper.FORMAT_DATE),
				new ReportHeader(5, "Técnico", CellHelper.FORMAT_TEXT)
		);

		ReportModel model = new ReportModel();
		model.setHeader(ListUtils.union(defaultHeader, header));
		model.setRows(chips.stream().map(chip -> {
			List<Object> cells = new ArrayList<>();
			cells.add(chip.getIccId());
			cells.add(chip.getOperator().getName()+" - "+chip.getCompany().getName());
			cells.add(chip.getRegionalOffice().getName());
			cells.add(chip.getTechnicianDelivery().getTechnicianName());
			cells.add(chip.getTechnicianDelivery().getDeliveryDate().toString());
			return cells;
		}).collect(Collectors.toList()));
		
		return model;
	}

}
