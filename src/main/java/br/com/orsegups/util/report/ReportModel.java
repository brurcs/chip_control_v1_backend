package br.com.orsegups.util.report;

import java.util.List;

public class ReportModel {
	private List<ReportHeader> header;
	private List<List<Object>> rows;

	public List<ReportHeader> getHeader() {
		return header;
	}

	public void setHeader(List<ReportHeader> header) {
		this.header = header;
	}

	public List<List<Object>> getRows() {
		return rows;
	}

	public void setRows(List<List<Object>> rows) {
		this.rows = rows;
	}
}
