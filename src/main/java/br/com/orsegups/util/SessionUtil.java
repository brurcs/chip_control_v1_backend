package br.com.orsegups.util;

import br.com.orsegups.configuration.security.RolesConstants;
import br.com.orsegups.configuration.security.UserPrincipal;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.service.RegionalOfficeService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

@Component
public final class SessionUtil {

    private final String REGIONAL_KEY = "regionalOffice";

    @Inject
    private RegionalOfficeService regionalOfficeService;

    public Long getCurrentUserId() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserPrincipal) {
                UserPrincipal springSecurityUser = (UserPrincipal) authentication.getPrincipal();
                return springSecurityUser.getId();
            }
        }
        return null;
    }

    public String getCurrentUserLogin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                return springSecurityUser.getUsername();
            } else if (authentication.getPrincipal() instanceof String) {
                return (String) authentication.getPrincipal();
            }
        }
        return null;
    }

    public List<RegionalOffice> getCurrentUserRegional() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getDetails() instanceof Map) {
                Map<String, Object> extraInfo = (Map) authentication.getDetails();
                if (extraInfo.containsKey(REGIONAL_KEY)) {
                    Object regional = extraInfo.get(REGIONAL_KEY);
                    if (regional instanceof List) {
                        List regionals = (List) regional;
                        if (!regionals.isEmpty()) {
                            if (regionals.get(0) instanceof RegionalOffice) {
                                return regionals;
                            } else {
                                List<Long> regionalIds = null;
                                if (regionals.get(0) instanceof Integer) {
                                    regionalIds = ((List<Integer>) regionals).stream().map(Long::new).collect(Collectors.toList());
                                } else if (regionals.get(0) instanceof Long) {
                                    regionalIds = (List<Long>) regionals;
                                } else if (regionals.get(0) instanceof String) {
                                    regionalIds = ((List<String>) regionals).stream().map(Long::valueOf).collect(Collectors.toList());
                                }

                                if (regionalIds != null && !regionalIds.isEmpty()) {
                                    List<RegionalOffice> regionalOffices = regionalOfficeService.findAllById(regionalIds);
                                    extraInfo.put(REGIONAL_KEY, regionalOffices);
                                    if (authentication instanceof UsernamePasswordAuthenticationToken)
                                        ((UsernamePasswordAuthenticationToken) authentication).setDetails(extraInfo);
                                    return regionalOffices;
                                }
                            }
                        }
                    }
                }
            }
        }
        return new ArrayList<>();
    }

    public List<Long> getCurrentUserRegionalIds() {
        List<RegionalOffice> regionalOffices = getCurrentUserRegional();
        if (regionalOffices.isEmpty()) return new ArrayList<>();
        return regionalOffices.stream().map(RegionalOffice::getId).collect(Collectors.toList());
    }

    public boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            return authorities != null && !authorities.isEmpty();
        }
        return false;
    }

    public boolean isCurrentUserInRole(String authority) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();
                return springSecurityUser.getAuthorities().contains(new SimpleGrantedAuthority(authority));
            }
        }
        return false;
    }

    public boolean isCurrentUserAdmin() {
        return isCurrentUserInRole(RolesConstants.ADMIN);
    }

    public boolean isCurrentUserRegionalManager() {
        return isCurrentUserInRole(RolesConstants.REGIONAL_MANAGER);
    }

    public boolean isCurrentUserTechnician() {
        return isCurrentUserInRole(RolesConstants.TECHNICIAN);
    }
}