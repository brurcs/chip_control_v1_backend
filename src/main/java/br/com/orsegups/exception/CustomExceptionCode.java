package br.com.orsegups.exception;

public enum CustomExceptionCode {

    ENTITY_NOT_FOUND(444),
    CUSTOM(555),
    GENERIC(500);

    private final Integer code;

    CustomExceptionCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
