package br.com.orsegups.exception;

public class ExceptionDTO {

    private String exceptionName;
    private String errorMessage;

    public ExceptionDTO(String exceptionName, String errorMessage) {
        this.exceptionName = exceptionName;
        this.errorMessage = errorMessage;
    }

    public String getExceptionName() {
        return exceptionName;
    }

    public void setExceptionName(String exceptionName) {
        this.exceptionName = exceptionName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
