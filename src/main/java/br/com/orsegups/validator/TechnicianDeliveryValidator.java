package br.com.orsegups.validator;

import br.com.orsegups.controller.technicianDelivery.TechnicianDeliveryVMI;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.util.SessionUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class TechnicianDeliveryValidator extends Validator {

    @Inject
    private SessionUtil sessionUtil;

    public void validateCreate(TechnicianDeliveryVMI vmi) {
        String technicianLabel = messages.get("entity.technicianDelivery.technician");
        notNull(vmi.getTechnician(), technicianLabel);

        String chipPlural = messages.get("entity.chip.plural");
        notEmpty(vmi.getChips(), chipPlural);

        validateTechnicianInUserRegional(vmi);
    }

    // Valida se o técnico pertence a regional do usuario logado
    public void validateTechnicianInUserRegional(TechnicianDeliveryVMI vmi) {
        String technicianRegionalAcronym = vmi.getTechnician().getName().split("-")[0].trim();
        List<String> regionalAcronyms = sessionUtil.getCurrentUserRegional().stream()
                .map(RegionalOffice::getAcronym).collect(Collectors.toList());
        if (!regionalAcronyms.contains(technicianRegionalAcronym)) {
            throw new CustomException(messages.get("validate.technicianDelivery.notAllowed"));
        }
    }

}
