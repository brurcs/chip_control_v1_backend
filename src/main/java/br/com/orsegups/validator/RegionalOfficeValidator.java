package br.com.orsegups.validator;

import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.util.SessionUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class RegionalOfficeValidator extends Validator {

    @Inject
    private SessionUtil sessionUtil;

    public void validateUserInRegional(RegionalOffice regional) {
        if (!sessionUtil.getCurrentUserRegional().contains(regional)) {
            throw new CustomException(messages.get("validate.regionalOffice.denied", regional.getName()));
        }
    }
}
