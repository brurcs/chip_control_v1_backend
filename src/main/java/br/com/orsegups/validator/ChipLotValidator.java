package br.com.orsegups.validator;

import br.com.orsegups.controller.chipLot.ChipLotVMI;
import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.entity.type.ChipLotStatusType;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.util.SessionUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class ChipLotValidator extends Validator {

    @Inject
    private SessionUtil sessionUtil;

    public void validateCreate(ChipLotVMI vmi) {
        chipStructureValidator(vmi);
    }

    private void chipStructureValidator(ChipLotVMI vmi) {
        String regionalLabel = messages.get("entity.chip.regional");
        String sendDateLabel = messages.get("entity.chipLot.sendDate");
        String expectedArrivalDateLabel = messages.get("entity.chipLot.expectedArrivalDate");
        String chipPlural = messages.get("entity.chip.plural");

        notNull(vmi.getRegional(), regionalLabel);
        notNull(vmi.getSendDate(), sendDateLabel);
        notNull(vmi.getExpectedArrivalDate(), expectedArrivalDateLabel);

        notEmpty(vmi.getChips(), chipPlural);

    }

    public void validateReceive(ChipLot chipLot) {
        validateStatusWaiting(chipLot);

        if (!sessionUtil.getCurrentUserRegional().contains(chipLot.getRegionalDestination())) {
            throw new CustomException(messages.get("validate.chipLot.receiveNotAllowed"));
        }
    }

    public void validateCancel(ChipLot chipLot) {
        validateStatusWaiting(chipLot);

        if (!sessionUtil.isCurrentUserAdmin()) {
            List<RegionalOffice> userRegionals = sessionUtil.getCurrentUserRegional();
            if (!userRegionals.contains(chipLot.getRegionalDestination())
                    && !userRegionals.contains(chipLot.getRegionalOrigin())) {
                throw new CustomException(messages.get("validate.chipLot.cancelNotAllowed"));
            }
        }
    }

    private void validateStatusWaiting(ChipLot chipLot) {
        if (!chipLot.getStatus().equals(ChipLotStatusType.WAITING)) {
            throw new CustomException(messages.get("validate.chipLot.alreadyReceived"));
        }
    }

}
