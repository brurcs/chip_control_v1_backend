package br.com.orsegups.validator;

import br.com.orsegups.controller.chip.ChipVMI;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.service.ChipLotItemService;
import br.com.orsegups.service.RegionalOfficeService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

@Component
public class ChipValidator extends Validator {

    @Inject
    private ChipLotItemService chipLotItemService;

    public void validateCreate(ChipVMI vmi) {
        chipStructureValidator(vmi);
    }

    private void chipStructureValidator(ChipVMI vmi) {
        String companyLabel = messages.get("entity.chip.company");
        String operatorLabel = messages.get("entity.chip.operator");
        String chipPlural = messages.get("entity.chip.plural");

        notNull(vmi.getCompany(), companyLabel);
        notNull(vmi.getOperator(), operatorLabel);

        notEmpty(vmi.getChips(), chipPlural);

    }

    public void validateAllChipsInStock(List<Chip> chips, RegionalOffice regionalOffice) {
        validateAllChipsInStock(chips, Collections.singletonList(regionalOffice));
    }

    public void validateAllChipsInStock(List<Chip> chips, List<RegionalOffice> regionalOffices) {
        chips.forEach(chip -> validateChipInRegionalStock(chip, regionalOffices));
    }

    public void validateChipInRegionalStock(Chip chip, List<RegionalOffice> regionalOffices) {
        if (!regionalOffices.contains(chip.getRegionalOffice())) {
            throw new CustomException(messages.get("validate.chip.notInStock", chip.getIccId()));
        } else {
            validateChipNotInCustomer(chip);
            validateChipNotWithTechnician(chip);
            validateChipNotInEquipment(chip);
        }
    }

    public void validateChipInRegionalStockOrTechnician(Chip chip, RegionalOffice regional) {
        validateChipInRegionalStockOrTechnician(chip, Collections.singletonList(regional));
    }

    public void validateChipInRegionalStockOrTechnician(Chip chip, List<RegionalOffice> regionalOffices) {
        validateChipNotInCustomer(chip);
        validateChipNotInEquipment(chip);
        validateChipInRegional(chip, regionalOffices);
    }

    public void validateChipNotInCustomer(Chip chip) {
        if (chip.getCustomerLink() != null) {
            throw new CustomException(messages.get("validate.chip.alreadyInCustomer", chip.getIccId()));
        }
    }

    public void validateChipNotInEquipment(Chip chip) {
        if (chip.getEquipmentLink() != null) {
            throw new CustomException(messages.get("validate.chip.alreadyInEquipment", chip.getIccId()));
        }
    }

    public void validateChipNotWithTechnician(Chip chip) {
        if (chip.getTechnicianDelivery() != null) {
            throw new CustomException(messages.get("validate.chip.alreadyWithTechnician", chip.getIccId()));
        }
    }

    public void validateChipInRegional(Chip chip, List<RegionalOffice> regionalOffices) {
        if (!regionalOffices.contains(chip.getRegionalOffice())) {
            throw new CustomException(messages.get("validate.chip.notInRegional", chip.getIccId()));
        }
    }

    public void validateChipWithTechnician(Chip chip, Long technicianId) {
        if (chip.getTechnicianDelivery() == null || !technicianId.equals(chip.getTechnicianDelivery().getTechnicianId())) {
            throw new CustomException(messages.get("validate.chip.notWithTechnician", chip.getIccId()));
        }
    }

    public void validateCancel(Chip chip) {
        if (chip.getRegionalOffice() == null || !RegionalOfficeService.STOCK_TO_CANCEL_ID.equals(chip.getRegionalOffice().getId())) {
            throw new CustomException(messages.get("validate.chip.notInStockToCancel", chip.getIccId()));
        }
    }
    public void validateCancellation(Chip chip) {
        if (chip.getCancellationDate() != null || chip.getRequestedCancellationDate() != null) {
            throw new CustomException(messages.get("validate.chip.alreadyInCancellation", chip.getIccId()));
        }
    }
    public void validateChipNotInOpenLot(Chip chip) {
        Integer numberOfOpenLots = chipLotItemService.countOpenLotWithChip(chip);

        if (numberOfOpenLots > 0) {
            throw new CustomException(messages.get("validate.chip.alreadyInLot", chip.getIccId()));
        }
    }
}
