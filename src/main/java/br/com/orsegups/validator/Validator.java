package br.com.orsegups.validator;

import br.com.orsegups.configuration.locale.Messages;
import br.com.orsegups.exception.CustomException;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collection;

@Component
public class Validator {

    @Inject
    protected Messages messages;

    protected void notNull(Object object, String... params) {
        if (object == null) throw new CustomException(messages.get("field.notNull", params));
    }

    protected void notEmpty(Collection collection, String... params) {
        if (collection == null || collection.size() < 1) throw new CustomException(messages.get("field.notEmpty", params));
    }

    protected void minLength(String str, int min, String... params) {
        if (str.length() < min) throw new CustomException(messages.get("field.minLength", params));
    }

    protected void maxLength(String str, int max, String... params) {
        if (str.length() > max) throw new CustomException(messages.get("field.maxLength", params));
    }

}
