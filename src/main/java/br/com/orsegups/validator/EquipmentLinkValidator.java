package br.com.orsegups.validator;

import br.com.orsegups.entity.EquipmentLink;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.exception.CustomException;
import br.com.orsegups.util.SessionUtil;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class EquipmentLinkValidator extends Validator {

    @Inject
    private SessionUtil sessionUtil;

    public void validateCancel(EquipmentLink equipmentLink) {
        if (sessionUtil.isCurrentUserRegionalManager()) {
            List<RegionalOffice> userRegionals = sessionUtil.getCurrentUserRegional();
            if (!userRegionals.contains(equipmentLink.getRegionalOffice())) {
                throw new CustomException(messages.get("validate.equipmentLink.cancel.notInRegional", equipmentLink.getRegionalOffice().getName()));
            }
        }
    }

    public void validateRegionalIsNotOffice(RegionalOffice regional) {
        if (regional.getOffice()) {
            throw new CustomException(messages.get("validate.equipmentLink.regionalNotAllowed", regional.getName()));
        }
    }

}
