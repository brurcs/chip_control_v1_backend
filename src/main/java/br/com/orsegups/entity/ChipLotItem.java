package br.com.orsegups.entity;

import br.com.orsegups.entity.type.ChipLotItemStatusType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class ChipLotItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private ChipLot chipLot;

    @ManyToOne(fetch = FetchType.EAGER)
    private Chip chip;

    @Type(type = "br.com.orsegups.entity.type.ChipLotItemStatusUserType")
    @Column(nullable = false)
    private ChipLotItemStatusType status;

    public ChipLotItem() {
        super();
        status = ChipLotItemStatusType.WAITING;
    }

    public ChipLotItem(ChipLot chipLot, Chip chip) {
        this();
        this.setChipLot(chipLot);
        this.chip = chip;
    }

    public ChipLotItem(ChipLot chipLot, Chip chip, ChipLotItemStatusType status) {
        this(chipLot, chip);
        this.status = status;
    }

    @Override
    public String toString() {
        return "ChipLotItem{" +
                "id=" + id +
                ", chip=" + chip +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChipLotItem that = (ChipLotItem) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ChipLot getChipLot() {
        return chipLot;
    }

    public void setChipLot(ChipLot chipLot) {
        if (chipLot != null) chipLot.addChipLotItem(this);
        this.chipLot = chipLot;
    }

    public Chip getChip() {
        return chip;
    }

    public void setChip(Chip chip) {
        this.chip = chip;
    }

    public ChipLotItemStatusType getStatus() {
        return status;
    }

    public void setStatus(ChipLotItemStatusType status) {
        this.status = status;
    }
}
