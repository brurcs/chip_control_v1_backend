package br.com.orsegups.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class RegionalOffice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 150)
    private String name;
    @Column(nullable = false, length = 20)
    private String acronym;
    @Column(nullable = false)
    private Boolean isOffice;
    private Integer standardStock;

    @Override
    public String toString() {
        return "RegionalOffice{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", acronym='" + acronym + '\'' +
                ", isOffice=" + isOffice +
                ", standardStock=" + standardStock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionalOffice that = (RegionalOffice) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Boolean getOffice() {
        return isOffice;
    }

    public void setOffice(Boolean office) {
        isOffice = office;
    }

    public Integer getStandardStock() {
        return standardStock;
    }

    public void setStandardStock(Integer standardStock) {
        this.standardStock = standardStock;
    }
}