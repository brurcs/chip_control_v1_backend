package br.com.orsegups.entity;

import br.com.orsegups.entity.type.TechnicianDeliveryStatusType;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class TechnicianDeliveryChip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private TechnicianDelivery technicianDelivery;

    @ManyToOne(fetch = FetchType.EAGER)
    private Chip chip;

    @Type(type = "br.com.orsegups.entity.type.TechnicianDeliveryStatusUserType")
    @Column(nullable = false)
    private TechnicianDeliveryStatusType status;

    public TechnicianDeliveryChip() {
        super();
        status = TechnicianDeliveryStatusType.DELIVERED;
    }

    public TechnicianDeliveryChip(TechnicianDelivery technicianDelivery, Chip chip) {
        this();
        this.setTechnicianDelivery(technicianDelivery);
        this.chip = chip;
    }

    public TechnicianDeliveryChip(TechnicianDelivery technicianDelivery, Chip chip, TechnicianDeliveryStatusType status) {
        this(technicianDelivery, chip);
        this.status = status;
    }

    @Override
    public String toString() {
        return "TechnicianDeliveryChip{" +
                "id=" + id +
                ", chip=" + chip +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TechnicianDeliveryChip that = (TechnicianDeliveryChip) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TechnicianDelivery getTechnicianDelivery() {
        return technicianDelivery;
    }

    public void setTechnicianDelivery(TechnicianDelivery technicianDelivery) {
        if (technicianDelivery != null) technicianDelivery.addTechnicianDeliveryChip(this);
        this.technicianDelivery = technicianDelivery;
    }

    public Chip getChip() {
        return chip;
    }

    public void setChip(Chip chip) {
        this.chip = chip;
    }

    public TechnicianDeliveryStatusType getStatus() {
        return status;
    }

    public void setStatus(TechnicianDeliveryStatusType status) {
        this.status = status;
    }
}
