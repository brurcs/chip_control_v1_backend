package br.com.orsegups.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
public class CustomerLink {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sigma_technician_id")
    private Long technicianId;

    @Column(name = "sigma_technician_name")
    private String technicianName;

    @Column(name = "sigma_order_id")
    private Long orderId;

    @Column(name = "sigma_company_id")
    private Long companyId;

    @Column(name = "sigma_central_id")
    private String centralId;

    @Column(name = "sigma_partition")
    private String partition;

    @Column(name = "sigma_business_name")
    private String businessName;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date installationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date removalDate;

    @Column(length = 500)
    private String removalReason;

    @ManyToOne(fetch = FetchType.EAGER)
    private Chip chip;

    @ManyToOne(fetch = FetchType.EAGER)
    private RegionalOffice regionalOffice;

    public CustomerLink() {
        super();
        this.installationDate = new Date();

    }

    public CustomerLink(Long companyId, String centralId, String partition, String businessName,
                        Chip chip, RegionalOffice regionalOffice) {
        this();
        this.companyId = companyId;
        this.centralId = centralId;
        this.partition = partition;
        this.businessName = businessName;
        this.chip = chip;
        this.regionalOffice = regionalOffice;
    }

    public CustomerLink(Long technicianId, String technicianName, Long orderId, Long companyId, String centralId,
                        String partition, String businessName, Chip chip, RegionalOffice regionalOffice) {
        this(companyId, centralId, partition, businessName, chip, regionalOffice);
        this.technicianId = technicianId;
        this.technicianName = technicianName;
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerLink customer = (CustomerLink) o;
        return id.equals(customer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(Long technicianId) {
        this.technicianId = technicianId;
    }

    public String getTechnicianName() {
        return technicianName;
    }

    public void setTechnicianName(String technicianName) {
        this.technicianName = technicianName;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCentralId() {
        return centralId;
    }

    public void setCentralId(String centralId) {
        this.centralId = centralId;
    }

    public String getPartition() {
        return partition;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public Date getRemovalDate() {
        return removalDate;
    }

    public void setRemovalDate(Date removalDate) {
        this.removalDate = removalDate;
    }

    public String getRemovalReason() {
        return removalReason;
    }

    public void setRemovalReason(String removalReason) {
        this.removalReason = removalReason;
    }

    public Chip getChip() {
        return chip;
    }

    public void setChip(Chip chip) {
        this.chip = chip;
    }

    public RegionalOffice getRegionalOffice() {
        return regionalOffice;
    }

    public void setRegionalOffice(RegionalOffice regionalOffice) {
        this.regionalOffice = regionalOffice;
    }

    public String getCustomerName() {
        return this.centralId + "[" + this.partition + "] " + this.businessName;
    }
}