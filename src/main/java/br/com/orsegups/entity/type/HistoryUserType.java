package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnumUserType;

public class HistoryUserType extends PersistentEnumUserType<HistoryType> {

    @Override
    public Class<HistoryType> returnedClass() {
        return HistoryType.class;
    }

}