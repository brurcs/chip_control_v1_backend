package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnum;


public enum ChipLotItemStatusType implements PersistentEnum {
    WAITING(1L),
    RECEIVED(2L),
    NOT_RECEIVED(3L),
    RECEIVED_WRONG(4L),
    CANCELED(5L);

    private final Long code;

    private ChipLotItemStatusType(Long code) {
        this.code = code;
    }

    public static ChipLotItemStatusType valueOf(Long code) {
        for (ChipLotItemStatusType chipLotItemStatusType : values()) {
            if (chipLotItemStatusType.code.equals(code))
                return chipLotItemStatusType;
        }
        return null;
    }

    public Long getCode() {
        return code;
    }

}

