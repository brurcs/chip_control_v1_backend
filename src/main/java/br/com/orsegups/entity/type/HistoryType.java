package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnum;

public enum HistoryType implements PersistentEnum {
    CHIP_CREATED(1L),
    LOT_SENT(2L),
    LOT_RECEIVED(3L),
    TECHNICIAN_DELIVERED(4L),
    TECHNICIAN_RETURNED(5L),
    CUSTOMER_LINK(6L),
    CUSTOMER_UNLINK(7L),
    EQUIPMENT_LINK(8L),
    EQUIPMENT_UNLINK(9L),
    REQUEST_CANCELLATION(10L),
    CANCELED(11L),
    UNDO_CANCELLATION(12L),
    LOT_CANCELED(13L);

    private final Long code;

    private HistoryType(Long code) {
        this.code = code;
    }

    public static HistoryType valueOf(Long code) {
        for (HistoryType historyType : values()) {
            if (historyType.code.equals(code))
                return historyType;
        }
        return null;
    }

    public Long getCode() {
        return code;
    }

}