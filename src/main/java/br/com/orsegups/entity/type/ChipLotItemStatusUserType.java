package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnumUserType;

public class ChipLotItemStatusUserType extends PersistentEnumUserType<ChipLotItemStatusType> {

    @Override
    public Class<ChipLotItemStatusType> returnedClass() {
        return ChipLotItemStatusType.class;
    }

}
