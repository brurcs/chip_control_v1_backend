package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnum;

public enum ChipLotStatusType implements PersistentEnum {
    WAITING(1L),
    RECEIVED(2L),
    CANCELED(3L);

    private final Long code;

    private ChipLotStatusType(Long code) {
        this.code = code;
    }

    public static ChipLotStatusType valueOf(Long code) {
        for (ChipLotStatusType chipLotStatusType : values()) {
            if (chipLotStatusType.code.equals(code))
                return chipLotStatusType;
        }
        return null;
    }

    public Long getCode() {
        return code;
    }

}