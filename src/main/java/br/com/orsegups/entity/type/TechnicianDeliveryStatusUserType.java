package br.com.orsegups.entity.type;

import br.com.orsegups.configuration.type.PersistentEnumUserType;

public class TechnicianDeliveryStatusUserType extends PersistentEnumUserType<TechnicianDeliveryStatusType> {

    @Override
    public Class<TechnicianDeliveryStatusType> returnedClass() {
        return TechnicianDeliveryStatusType.class;
    }

}