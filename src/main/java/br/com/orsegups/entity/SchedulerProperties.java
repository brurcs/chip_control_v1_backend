package br.com.orsegups.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SchedulerProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String dailyCancelCutoffDate;

    public SchedulerProperties() {
        super();
    }

    public SchedulerProperties(Long id, String dailyCancelCutoffDate) {
        this();
        this.id = id;
        this.dailyCancelCutoffDate = dailyCancelCutoffDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDailyCancelCutoffDate() {
        return dailyCancelCutoffDate;
    }

    public void setDailyCancelCutoffDate(String dailyCancelCutoffDate) {
        this.dailyCancelCutoffDate = dailyCancelCutoffDate;
    }
}
