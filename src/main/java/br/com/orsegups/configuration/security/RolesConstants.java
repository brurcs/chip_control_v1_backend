package br.com.orsegups.configuration.security;

import java.util.Arrays;
import java.util.List;

public final class RolesConstants {

    public static final String ADMIN = "CHIP_administrador";
    public static final String REGIONAL_MANAGER = "CHIP_gerente_regional";
    public static final String TECHNICIAN = "CHIP_tecnico";
    public static final String VETTI = "CHIP_vetti";

    public static final List<String> ALL_ROLES =  Arrays.asList(ADMIN, REGIONAL_MANAGER, TECHNICIAN, VETTI);

    public static final List<String> CAS_ROLES =  Arrays.asList(ADMIN, REGIONAL_MANAGER);
    public static final List<String> SIGMA_ROLES =  Arrays.asList(TECHNICIAN);
}
