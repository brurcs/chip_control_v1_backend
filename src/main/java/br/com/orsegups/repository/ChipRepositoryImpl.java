package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.repository.utils.AbstractRepositoryImpl;
import br.com.orsegups.repository.utils.QueryObject;
import br.com.orsegups.service.RegionalOfficeService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

@Component
public class ChipRepositoryImpl extends AbstractRepositoryImpl<Chip> implements ChipRepositoryInterface {

    public PageDTO<Chip> findInStockByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalIds, "regional");
        return executePagedQuery(queryObject, pageable, "c");
    }

    public PageDTO<Chip> findInCustomerByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalIds, "customer");
        return executePagedQuery(queryObject, pageable, "c");
    }

    public PageDTO<Chip> findInTechnicianByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalIds, "technician");
        return executePagedQuery(queryObject, pageable, "c");
    }

    public PageDTO<Chip> findInEquipmentByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalIds, "equipment");
        return executePagedQuery(queryObject, pageable, "c");
    }

    public PageDTO<Chip> findToCancelByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getCancelQueryObject(searchText, regionalIds, "inCancellation");
        return executePagedQuery(queryObject, pageable, "c");
    }

    public PageDTO<Chip> findCanceledByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getCancelQueryObject(searchText, regionalIds, "canceled");
        return executePagedQuery(queryObject, pageable, "c");
    }

    public QueryObject getCancelQueryObject(String searchText, List<Long> regionalIds, String type) {
        QueryObject queryObject = new QueryObject();
        StringBuilder queryString = new StringBuilder(" FROM chip c ");
        queryString.append(" INNER JOIN operator o ON o.id = c.operator_id WHERE 1=1 ");

        if (StringUtils.hasText(type)) {
            queryString.append(" AND ");
            if (type.equals("inCancellation")) {
                queryString.append(" c.regional_office_id = :regionalId ");
                queryObject.putParameter("regionalId", RegionalOfficeService.STOCK_TO_CANCEL_ID);
            } else if (type.equals("canceled")) {
                queryString.append(" c.cancellation_date IS NOT NULL ");
            }
        }

        if (StringUtils.hasText(searchText)) {
            queryString.append(" AND (c.icc_id LIKE :searchText OR o.name LIKE :searchText) ");
            queryObject.putParameter("searchText", "%" + searchText + "%");
        }

        if (regionalIds != null && regionalIds.size() > 0) {
            queryString.append(" AND c.canceled_from_regional_id IN :regionalIds ");
            queryObject.putParameter("regionalIds", regionalIds);
        }

        queryObject.setQueryString(queryString.toString());
        return queryObject;
    }

    private QueryObject getQueryObject(String searchText, List<Long> regionalIds, String type) {
        QueryObject queryObject = new QueryObject();
        StringBuilder queryString = new StringBuilder(" FROM chip c ");
        queryString.append(" LEFT JOIN customer_link cl ON cl.id = c.customer_link_id ");
        queryString.append(" LEFT JOIN technician_delivery td ON td.id = c.technician_delivery_id ");
        queryString.append(" LEFT JOIN equipment_link el ON el.id = c.equipment_link_id ");
        queryString.append(" INNER JOIN regional_office ro ON ro.id = c.regional_office_id ");
        queryString.append(" INNER JOIN operator o ON o.id = c.operator_id WHERE 1=1 ");

        if (StringUtils.hasText(type)) {
            queryString.append(" AND ");
            if (type.equals("technician")) {
                queryString.append(" c.customer_link_id IS NULL AND c.equipment_link_id IS NULL AND c.technician_delivery_id IS NOT NULL ");
            } else if (type.equals("customer")) {
                queryString.append(" c.technician_delivery_id IS NULL AND c.customer_link_id IS NOT NULL ");
            } else if (type.equals("equipment")) {
                queryString.append(" c.customer_link_id IS NULL AND c.technician_delivery_id IS NULL AND c.equipment_link_id IS NOT NULL ");
            } else {
                queryString.append(" c.technician_delivery_id IS NULL AND c.customer_link_id IS NULL AND c.equipment_link_id IS NULL ");
                queryString.append(" AND c.regional_office_id <> " + RegionalOfficeService.STOCK_TO_CANCEL_ID);
            }
        }

        if (regionalIds != null && regionalIds.size() > 0) {
            queryString.append(" AND c.regional_office_id IN :regionalIds ");
            queryObject.putParameter("regionalIds", regionalIds);
        }

        if (StringUtils.hasText(searchText)) {
            queryString.append(" AND (");
            queryString.append(" c.icc_id LIKE :searchText ");
            queryString.append(" OR o.name LIKE :searchText ");
            queryString.append(" OR CONCAT(cl.sigma_central_id, '[', cl.sigma_partition, '] ', cl.sigma_business_name) LIKE :searchText ");
            queryString.append(" OR td.sigma_technician_name LIKE :searchText ");
            queryString.append(" OR CONCAT(ro.acronym, ' - ', ro.name) LIKE :searchText ");
            queryString.append(" OR el.identifier LIKE :searchText ");
            queryString.append(" ) ");
            queryObject.putParameter("searchText", "%" + searchText + "%");
        }

        queryObject.setQueryString(queryString.toString());
        return queryObject;
    }


}
