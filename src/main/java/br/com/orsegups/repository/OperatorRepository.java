package br.com.orsegups.repository;

import br.com.orsegups.entity.Operator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperatorRepository extends JpaRepository<Operator, Long> {

    Operator findByName(String name);

}
