package br.com.orsegups.repository;

import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.RegionalOffice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChipRepository extends JpaRepository<Chip, Long>, ChipRepositoryInterface {

    Optional<Chip> findFirstByIccId(String iccId);

    List<Chip> findByIccIdIn(List<String> iccIds);

    List<Chip> findByTechnicianDeliveryTechnicianId(Long id);

    Integer countByTechnicianDeliveryTechnicianId(Long id);

    List<Chip> findByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(Long regionalOfficeId);

    List<Chip> findByRegionalOfficeInAndCustomerLinkIsNullAndEquipmentLinkIsNull(List<RegionalOffice> regionalOffices);

    Integer countByCancellationDateIsNull();

    Integer countByCancellationDateIsNotNull();

    Integer countByRegionalOfficeId(Long id);

    Integer countByRegionalOfficeIsOffice(Boolean isOffice);

    Integer countByCustomerLinkIsNotNull();

    @Query("SELECT count(c) FROM Chip c JOIN c.regionalOffice ro " +
            " WHERE c.cancellationDate IS NULL AND c.customerLink IS NULL " +
            " AND c.technicianDelivery IS NULL AND c.equipmentLink IS NULL AND ro.isOffice = 1")
    Integer countChipsInOfficesStock();

    Integer countByTechnicianDeliveryIsNotNullAndRegionalOfficeIsOfficeTrue();

    Integer countByRegionalOfficeIdAndEquipmentLinkIsNotNullAndCustomerLinkIsNull(Long regionalId);

    Integer countByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(Long regionalId);

}
