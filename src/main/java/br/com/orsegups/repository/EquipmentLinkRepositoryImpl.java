package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.EquipmentLink;
import br.com.orsegups.repository.utils.AbstractRepositoryImpl;
import br.com.orsegups.repository.utils.QueryObject;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Component
public class EquipmentLinkRepositoryImpl extends AbstractRepositoryImpl<EquipmentLink> implements EquipmentLinkRepositoryInterface {

    public PageDTO<EquipmentLink> findLinkedByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        QueryObject queryObject = getQueryObject(searchText, regionalIds);
        return executePagedQuery(queryObject, pageable, "el");
    }

    private QueryObject getQueryObject(String searchText, List<Long> regionalIds) {
        QueryObject queryObject = new QueryObject();
        StringBuilder queryString = new StringBuilder(" FROM equipment_link el INNER JOIN chip c ON c.id = el.chip_id " +
                " WHERE el.removal_date IS NULL AND el.regional_office_id IN :regionalIds ");
        queryObject.putParameter("regionalIds", regionalIds);

        if (StringUtils.hasText(searchText)) {
            queryString.append(" AND (c.icc_id LIKE :searchText ");
            queryString.append(" OR el.identifier LIKE :searchText) ");
            queryObject.putParameter("searchText", "%" + searchText + "%");
        }

        queryObject.setQueryString(queryString.toString());
        return queryObject;
    }


}
