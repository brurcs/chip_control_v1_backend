package br.com.orsegups.repository.utils;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.util.PaginationUtil;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.List;

@Component
public class AbstractRepositoryImpl<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    protected PageDTO<T> executePagedQuery(QueryObject queryObject, Pageable pageable, String selectAlias) {
        Long totalItems = executeCountQuery(queryObject, selectAlias);
        Integer totalPages = PaginationUtil.getTotalOfPages(totalItems, pageable.getPageSize());
        pageable = PaginationUtil.checkMaxPage(totalPages, pageable);
        List<T> items = executeQuery(queryObject, pageable, selectAlias);
        return new PageDTO<>(totalPages, totalItems, items);
    }

    protected Long executeCountQuery(QueryObject queryObject, String selectAlias) {
        Query query = entityManager.createNativeQuery("SELECT COUNT(DISTINCT " + selectAlias + ".id) " + queryObject.getQueryString());
        queryObject.getParameters().forEach(query::setParameter);
        return ((BigInteger) query.getSingleResult()).longValue();
    }

    protected List<T> executeQuery(QueryObject queryObject, Pageable pageable, String selectAlias) {
        StringBuilder orderBy = new StringBuilder();
        pageable.getSort().forEach(order -> {
            orderBy.append(StringUtils.hasText(orderBy.toString()) ? " , " : " ORDER BY ");
            orderBy.append(order.getProperty() + (order.isAscending() ? " ASC " : " DESC "));
        });

        Query query = entityManager.createNativeQuery("SELECT DISTINCT " + selectAlias + ".* " + queryObject.getQueryString()
                + " GROUP BY " + selectAlias + ".id  " + orderBy.toString(), getGenericEntityClass());
        queryObject.getParameters().forEach(query::setParameter);
        query.setMaxResults(pageable.getPageSize());
        query.setFirstResult(pageable.getPageSize() * (pageable.getPageNumber()));
        return query.getResultList();
    }


    private Class getGenericEntityClass() {
        return ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
    }
}
