package br.com.orsegups.repository;

import br.com.orsegups.entity.TechnicianDelivery;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnicianDeliveryRepository extends JpaRepository<TechnicianDelivery, Long> {

}
