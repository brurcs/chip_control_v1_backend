package br.com.orsegups.repository;

import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.CustomerLink;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CustomerLinkRepositoryInterface {

    PageDTO<CustomerLink> findInstalledByFilters(String searchText, List<Long> regionalIds, Pageable pageable);

}
