package br.com.orsegups.repository;

import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.ChipLotItem;
import br.com.orsegups.entity.type.ChipLotItemStatusType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChipLotItemRepository extends JpaRepository<ChipLotItem, Long> {

    Integer countByStatusAndChip(ChipLotItemStatusType status, Chip chip);

}
