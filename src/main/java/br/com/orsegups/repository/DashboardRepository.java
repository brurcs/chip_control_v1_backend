package br.com.orsegups.repository;

import br.com.orsegups.dto.OperatorDTO;
import br.com.orsegups.dto.OperatorStatsDTO;
import br.com.orsegups.dto.RegionalOfficeDTO;
import br.com.orsegups.dto.RegionalStatsDTO;
import br.com.orsegups.service.RegionalOfficeService;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Component
public class DashboardRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public List<RegionalStatsDTO> getStatsGroupByRegional(int isOffice) {
        StringBuilder strQuery = new StringBuilder();
        strQuery.append(" SELECT r.id, r.acronym, r.name, r.standard_stock, ");
        strQuery.append(" COALESCE(a.amount, 0) AS total, ");
        strQuery.append(" COALESCE(s.amount, 0) AS stock, ");
        strQuery.append(" COALESCE(t.amount, 0) AS technician, ");
        strQuery.append(" COALESCE(c.amount, 0) AS customer ");
        strQuery.append(" FROM regional_office r ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("   SELECT ro.id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("   INNER JOIN regional_office ro ON ro.id = c.regional_office_id ");
        strQuery.append("   WHERE ro.is_office = :isOffice ");
        strQuery.append("   GROUP BY ro.id ");
        strQuery.append(" ) a ON a.id = r.id ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("   SELECT ro.id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("   INNER JOIN regional_office ro ON ro.id = c.regional_office_id ");
        strQuery.append("   WHERE ro.is_office = :isOffice AND c.customer_link_id IS NULL ");
        strQuery.append("   AND c.technician_delivery_id IS NULL AND c.equipment_link_id IS NULL ");
        strQuery.append("   GROUP BY ro.id ");
        strQuery.append(" ) s ON s.id = r.id ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("   SELECT ro.id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("   INNER JOIN regional_office ro ON ro.id = c.regional_office_id ");
        strQuery.append("   WHERE ro.is_office = :isOffice AND c.technician_delivery_id IS NOT NULL ");
        strQuery.append("   GROUP BY ro.id ");
        strQuery.append(" ) t ON t.id = r.id ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("   SELECT ro.id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("   INNER JOIN regional_office ro ON ro.id = c.regional_office_id ");
        strQuery.append("   WHERE ro.is_office = :isOffice AND c.customer_link_id IS NOT NULL ");
        strQuery.append("   GROUP BY ro.id ");
        strQuery.append(" ) c ON c.id = r.id ");
        strQuery.append(" WHERE r.is_office = :isOffice ");
        strQuery.append(" ORDER BY r.name ASC ");

        Query query = entityManager.createNativeQuery(strQuery.toString());
        query.setParameter("isOffice", isOffice);
        List<Object[]> result = (List) query.getResultList();

        List<RegionalStatsDTO> regionalStatsList = new ArrayList<>();
        result.forEach(rowValues -> {
            RegionalOfficeDTO regionalOffice = new RegionalOfficeDTO();
            regionalOffice.setId(((Number) rowValues[0]).longValue());
            regionalOffice.setAcronym(String.valueOf(rowValues[1]));
            regionalOffice.setName(String.valueOf(rowValues[2]));
            regionalOffice.setStandardStock(((Number) rowValues[3]).intValue());

            RegionalStatsDTO regionalStats = new RegionalStatsDTO(regionalOffice);
            regionalStats.setOfficeAmount(((Number) rowValues[4]).intValue());
            regionalStats.setStockAmount(((Number) rowValues[5]).intValue());
            regionalStats.setTechnicianAmount(((Number) rowValues[6]).intValue());
            regionalStats.setCustomerAmount(((Number) rowValues[7]).intValue());

            regionalStatsList.add(regionalStats);
        });

        return regionalStatsList;
    }

    public List<OperatorStatsDTO> getActiveOperatorStats() {
        StringBuilder strQuery = new StringBuilder();
        strQuery.append(" SELECT o.id, o.name, ");
        strQuery.append(" COALESCE(a.amount, 0) AS active, ");
        strQuery.append(" COALESCE(c.amount, 0) AS cancellation ");
        strQuery.append(" FROM operator o ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("   SELECT c.operator_id AS id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("   WHERE c.cancellation_date IS NULL AND c.regional_office_id <> :toCancelRegional ");
        strQuery.append("   GROUP BY c.operator_id ");
        strQuery.append(" ) a ON a.id = o.id ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("   SELECT c.operator_id AS id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("   WHERE c.cancellation_date IS NULL AND c.regional_office_id = :toCancelRegional  ");
        strQuery.append("   GROUP BY c.operator_id ");
        strQuery.append(" ) c ON c.id = o.id ");
        strQuery.append(" WHERE o.active IS TRUE ");
        strQuery.append(" ORDER BY o.name ASC ");

        Query query = entityManager.createNativeQuery(strQuery.toString());
        query.setParameter("toCancelRegional", RegionalOfficeService.STOCK_TO_CANCEL_ID);
        List<Object[]> result = (List) query.getResultList();

        List<OperatorStatsDTO> operatorStatsList = new ArrayList<>();
        result.forEach(rowValues -> {
            OperatorDTO operator = new OperatorDTO();
            operator.setId(((Number) rowValues[0]).longValue());
            operator.setName(String.valueOf(rowValues[1]));

            OperatorStatsDTO operatorStats = new OperatorStatsDTO(operator);
            operatorStats.setActiveAmount(((Number) rowValues[2]).intValue());
            operatorStats.setCancellationAmount(((Number) rowValues[3]).intValue());

            operatorStatsList.add(operatorStats);
        });

        return operatorStatsList;
    }

    public List<RegionalStatsDTO> getStockStatsByRegional() {
        StringBuilder strQuery = new StringBuilder();

        strQuery.append(" SELECT r.id, r.acronym, r.name, r.standard_stock, ");
        strQuery.append("        COALESCE(s.amount, 0) AS stock, ");
        strQuery.append("        COALESCE(it.amount, 0) AS in_transit ");
        strQuery.append(" FROM regional_office r ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("       SELECT ro.id, COUNT(c.id) AS amount FROM chip c ");
        strQuery.append("       INNER JOIN regional_office ro ON ro.id = c.regional_office_id ");
        strQuery.append("       WHERE c.customer_link_id IS NULL AND c.equipment_link_id IS NULL AND ro.standard_stock <> 0 ");
        strQuery.append("       GROUP BY ro.id ");
        strQuery.append(" ) s ON s.id = r.id ");
        strQuery.append(" LEFT JOIN ( ");
        strQuery.append("       SELECT ro.id, SUM(cl.sent_amount) AS amount FROM chip_lot cl ");
        strQuery.append("       INNER JOIN regional_office ro ON ro.id = cl.regional_destination_id ");
        strQuery.append("       WHERE cl.status = 1 AND ro.standard_stock <> 0 ");
        strQuery.append("       GROUP BY ro.id ");
        strQuery.append(" ) it ON it.id = r.id ");
        strQuery.append(" WHERE r.standard_stock <> 0 ");
        strQuery.append(" ORDER BY r.name ASC ");

        Query query = entityManager.createNativeQuery(strQuery.toString());
        List<Object[]> result = (List) query.getResultList();

        List<RegionalStatsDTO> regionalStatsList = new ArrayList<>();
        result.forEach(rowValues -> {
            RegionalOfficeDTO regionalOffice = new RegionalOfficeDTO();
            regionalOffice.setId(((Number) rowValues[0]).longValue());
            regionalOffice.setAcronym(String.valueOf(rowValues[1]));
            regionalOffice.setName(String.valueOf(rowValues[2]));
            regionalOffice.setStandardStock(((Number) rowValues[3]).intValue());

            RegionalStatsDTO regionalStats = new RegionalStatsDTO(regionalOffice);
            regionalStats.setStockAmount(((Number) rowValues[4]).intValue());
            regionalStats.setInTransitAmount(((Number) rowValues[5]).intValue());

            regionalStatsList.add(regionalStats);
        });

        return regionalStatsList;
    }
}
