package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.*;
import br.com.orsegups.repository.ChipRepository;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ChipControlService
public class ChipService extends AbstractService<Chip, ChipRepository> {

    public List<Chip> createAll(List<ChipDTO> chipDTOs, Company company, Operator operator, RegionalOffice regionalOffice) {
        List<Chip> chips = chipDTOs.stream()
                .filter(chipDTO -> chipDTO.getIccId() != null)
                .map(chipDTO -> buildFromDTO(chipDTO, company, operator, regionalOffice)).collect(Collectors.toList());

        return repository.saveAll(chips);
    }

    public Chip findByIccId(String iccId) {
        return repository.findFirstByIccId(iccId).orElseThrow(
            () ->  new EntityNotFoundException(messages.get("entity.chip.notFound", iccId))
        );
    }

    public List<Chip> findByIccIds(List<String> iccIds) {
        return repository.findByIccIdIn(iccIds);
    }

    public Optional<Chip> findOptionalByIccId(String iccId) {
        return repository.findFirstByIccId(iccId);
    }

    public Chip buildFromDTO(ChipDTO chipDTO, Company company, Operator operator, RegionalOffice regionalOffice) {
        return new Chip(chipDTO.getIccId(), company, operator, regionalOffice);
    }

    public List<Chip> updateTechnician(List<Chip> chips, TechnicianDelivery technicianDelivery) {
        chips.forEach(chip -> chip.setTechnicianDelivery(technicianDelivery));
        return repository.saveAll(chips);
    }

    public List<Chip> findByTechnicianId(Long id) {
        return repository.findByTechnicianDeliveryTechnicianId(id);
    }

    public Integer countByTechnicianId(Long id) {
        return repository.countByTechnicianDeliveryTechnicianId(id);
    }

    public List<Chip> findByRegional(List<RegionalOffice> regionalOffices) {
        return repository.findByRegionalOfficeInAndCustomerLinkIsNullAndEquipmentLinkIsNull(regionalOffices);
    }

    public List<Chip> findByRegional(Long regionalOfficeId) {
        return repository.findByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(regionalOfficeId);
    }

    public PageDTO<Chip> findInStockByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findInStockByFilters(searchText, regionalIds, pageable);
    }

    public PageDTO<Chip> findInTechnicianByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findInTechnicianByFilters(searchText, regionalIds, pageable);
    }

    public PageDTO<Chip> findInCustomerByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findInCustomerByFilters(searchText, regionalIds, pageable);
    }

    public PageDTO<Chip> findInEquipmentByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findInEquipmentByFilters(searchText, regionalIds, pageable);
    }

    public PageDTO<Chip> findToCancelByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findToCancelByFilters(searchText, regionalIds,  pageable);
    }

    public PageDTO<Chip> findCanceledByFilters(String searchText, List<Long> regionalIds, Pageable pageable) {
        return repository.findCanceledByFilters(searchText, regionalIds, pageable);
    }

    /* COUNTS */
    public Integer countActiveChips() {
        return repository.countByCancellationDateIsNull();
    }

    public Integer countCanceledChips() {
        return repository.countByCancellationDateIsNotNull();
    }

    public Integer countInCustomer() {
        return repository.countByCustomerLinkIsNotNull();
    }

    public Integer countInOfficesStock() {
        return repository.countChipsInOfficesStock();
    }

    public Integer countInOfficesWithTechnician() {
        return repository.countByTechnicianDeliveryIsNotNullAndRegionalOfficeIsOfficeTrue();
    }

    public Integer countInStockTI() {
        return repository.countByRegionalOfficeId(RegionalOfficeService.STOCK_TI_ID);
    }

    public Integer countInCancellationChips() {
        return repository.countByRegionalOfficeId(RegionalOfficeService.STOCK_TO_CANCEL_ID);
    }

    public Integer countInTrackingStock() {
        return repository.countByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(RegionalOfficeService.TRACKING_ID);
    }

    public Integer countIdentifiedInTracking() {
        return repository.countByRegionalOfficeIdAndEquipmentLinkIsNotNullAndCustomerLinkIsNull(RegionalOfficeService.TRACKING_ID);
    }

    public Integer countInDeggyStock() {
        return repository.countByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(RegionalOfficeService.DEGGY_ID);
    }

    public Integer countIdentifiedInDeggy() {
        return repository.countByRegionalOfficeIdAndEquipmentLinkIsNotNullAndCustomerLinkIsNull(RegionalOfficeService.DEGGY_ID);
    }

    public Integer countInNextiStock() {
        return repository.countByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(RegionalOfficeService.NEXTI_ID);
    }

    public Integer countIdentifiedInNexti() {
        return repository.countByRegionalOfficeIdAndEquipmentLinkIsNotNullAndCustomerLinkIsNull(RegionalOfficeService.NEXTI_ID);
    }

    public Integer countInPrimeStock() {
        return repository.countByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(RegionalOfficeService.ORSEGUPS_PRIME_ID)
                + repository.countByRegionalOfficeIdAndCustomerLinkIsNullAndEquipmentLinkIsNull(RegionalOfficeService.EXECUTIVES_PRIME_ID);
    }

    public Integer countIdentifiedInPrime() {
        return repository.countByRegionalOfficeIdAndEquipmentLinkIsNotNullAndCustomerLinkIsNull(RegionalOfficeService.ORSEGUPS_PRIME_ID)
                + repository.countByRegionalOfficeIdAndEquipmentLinkIsNotNullAndCustomerLinkIsNull(RegionalOfficeService.EXECUTIVES_PRIME_ID);
    }


    public Integer countInOffices() {
        return repository.countByRegionalOfficeIsOffice(true);
    }

    public Integer countInEquipments() {
        return repository.countByRegionalOfficeIsOffice(false);
    }

}
