package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.TechnicianDelivery;
import br.com.orsegups.entity.TechnicianDeliveryChip;
import br.com.orsegups.repository.TechnicianDeliveryChipRepository;

@ChipControlService
public class TechnicianDeliveryChipService extends AbstractService<TechnicianDeliveryChip, TechnicianDeliveryChipRepository> {

    public TechnicianDeliveryChip findByChipAndTechnicianDelivery(Chip chip, TechnicianDelivery technicianDelivery) {
        return repository.findByChipAndTechnicianDelivery(chip, technicianDelivery);
    }

}
