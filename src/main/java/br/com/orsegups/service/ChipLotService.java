package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.PageDTO;
import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.type.ChipLotStatusType;
import br.com.orsegups.repository.ChipLotRepository;
import org.springframework.data.domain.Pageable;

import java.util.List;

@ChipControlService
public class ChipLotService extends AbstractService<ChipLot, ChipLotRepository> {

    public List<ChipLot> findAll() {
        return repository.findAllByOrderBySendDateDesc();
    }

    public PageDTO<ChipLot> findListToReceiveByFilters(String searchText, List<Long> regionalDestinationIds, Boolean lateOnly, Pageable pageable) {
        return repository.findListToReceiveByFilters(searchText, regionalDestinationIds, lateOnly, pageable);
    }

    public PageDTO<ChipLot> findSentListByFilters(String searchText, List<Long> regionalOriginIds, List<Long> regionalDestinationIds, Boolean lateOnly, Pageable pageable) {
        return repository.findSentListByFilters(searchText, regionalOriginIds, regionalDestinationIds, lateOnly, pageable);
    }

    public Integer countWaitingByRegional(Long regionalId) {
        return repository.countByStatusAndRegionalDestinationId(ChipLotStatusType.WAITING, regionalId);
    }

}
