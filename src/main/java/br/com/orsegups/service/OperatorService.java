package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.dto.OperatorDTO;
import br.com.orsegups.entity.Operator;
import br.com.orsegups.repository.OperatorRepository;

@ChipControlService
public class OperatorService extends AbstractService<Operator, OperatorRepository> {

    public Operator getFromDTO(OperatorDTO operatorDTO) {
        return findById(operatorDTO.getId());
    }

    public Operator findByName(String name) {
        return repository.findByName(name);
    }

}
