package br.com.orsegups.service;

import br.com.orsegups.configuration.annotation.ChipControlService;
import br.com.orsegups.entity.TechnicianDelivery;
import br.com.orsegups.repository.TechnicianDeliveryRepository;

@ChipControlService
public class TechnicianDeliveryService extends AbstractService<TechnicianDelivery, TechnicianDeliveryRepository> {

}
