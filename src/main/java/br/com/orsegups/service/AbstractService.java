package br.com.orsegups.service;

import br.com.orsegups.configuration.locale.Messages;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

public class AbstractService<E extends Object, T extends JpaRepository> {

    @Inject
    protected T repository;
    @Inject
    protected Messages messages;

    public E save(E entity) {
        return (E) repository.save(entity);
    }

    public List<E> saveAll(List<E> entityList) {
        return repository.saveAll(entityList);
    }

    public E saveAndFlush(E entity) {
        return (E) repository.saveAndFlush(entity);
    }

    public List<E> findAll() {
        return repository.findAll();
    }

    public List<E> findAllById(List<Long> ids) {
        return repository.findAllById(ids);
    }

    public E findById(Long id) {
        return validateNotNull((Optional<E>) repository.findById(id));
    }

    protected E validateNotNull(Optional<E> entity) {
        return entity.orElseThrow(() -> new EntityNotFoundException(
            messages.get("entity." + getGenericEntityClass().getSimpleName().toLowerCase() + ".notFound")
        ));
    }

    private Class getGenericEntityClass() {
        return ((Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
    }

}
