package br.com.orsegups.controller.report;

import br.com.orsegups.configuration.annotation.ChipControlController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@ChipControlController("/api/report")
public class ReportController {

    @Inject
    private ReportFacade reportFacade;

    @PostMapping(value = "/inCancellation")
    public ResponseEntity<?> reportInCancellation(@RequestBody ReportVMI vmi) {
        return new ResponseEntity<>(reportFacade.reportInCancellation(vmi), getFileHeader("a cancelar.xlsx"), HttpStatus.OK);
    }

    private HttpHeaders getFileHeader(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=" + name);
        return headers;
    }
}
