package br.com.orsegups.controller.chipLot;

import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.RegionalOfficeDTO;
import br.com.orsegups.dto.TableOptionDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class ChipLotVMI {

    private List<ChipDTO> chips;
    private RegionalOfficeDTO regional;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date sendDate;
    @JsonFormat(pattern="dd/MM/yyyy HH:mm:ss")
    private Date expectedArrivalDate;

    private String searchText;
    private TableOptionDTO tableOption;
    private Boolean lateOnly;

    public List<ChipDTO> getChips() {
        return chips;
    }

    public void setChips(List<ChipDTO> chips) {
        this.chips = chips;
    }

    public RegionalOfficeDTO getRegional() {
        return regional;
    }

    public void setRegional(RegionalOfficeDTO regional) {
        this.regional = regional;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public Date getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(Date expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }

    public Boolean getLateOnly() {
        return lateOnly;
    }

    public void setLateOnly(Boolean lateOnly) {
        this.lateOnly = lateOnly;
    }
}
