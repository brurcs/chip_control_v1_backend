package br.com.orsegups.controller.chipLot;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.*;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.ChipLot;
import br.com.orsegups.entity.ChipLotItem;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.entity.type.ChipLotItemStatusType;
import br.com.orsegups.entity.type.ChipLotStatusType;
import br.com.orsegups.service.ChipHistoryService;
import br.com.orsegups.service.ChipLotService;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.service.RegionalOfficeService;
import br.com.orsegups.util.DateUtil;
import br.com.orsegups.util.SessionUtil;
import br.com.orsegups.validator.ChipLotValidator;
import br.com.orsegups.validator.ChipValidator;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class ChipLotFacade {

    @Inject
    private ChipService chipService;
    @Inject
    private ChipLotService chipLotService;
    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private ChipHistoryService chipHistoryService;
    @Inject
    private ChipLotValidator chipLotValidator;
    @Inject
    private ChipValidator chipValidator;
    @Inject
    private SessionUtil sessionUtil;

    public ChipLotDTO create(ChipLotVMI vmi) {
        chipLotValidator.validateCreate(vmi);
        ChipLot chipLot = new ChipLot();

        List<Long> chipIds = vmi.getChips().stream().map(ChipDTO::getId).collect(Collectors.toList());
        List<Chip> chips = chipService.findAllById(chipIds);
        RegionalOffice regionalOrigin = regionalOfficeService.findStockTI();
        chipValidator.validateAllChipsInStock(chips, regionalOrigin);
        chips.forEach(chip -> new ChipLotItem(chipLot, chip));

        chipLot.setSendDate(DateUtil.getAtMiddayTime(vmi.getSendDate()));
        chipLot.setExpectedArrivalDate(DateUtil.getAtMiddayTime(vmi.getExpectedArrivalDate()));
        chipLot.setRegionalOrigin(regionalOrigin);
        chipLot.setRegionalDestination(regionalOfficeService.findById(vmi.getRegional().getId()));
        chipLot.setSentAmount(chipIds.size());
        chipLot.setStatus(ChipLotStatusType.WAITING);
        chipLotService.save(chipLot);

        chipHistoryService.lotSent(chips, chipLot);
        return new ChipLotDTO(chipLot);
    }

    public ChipLotDTO receive(ChipLotDTO chipLotDTO) {
        ChipLot chipLot = chipLotService.findById(chipLotDTO.getId());
        chipLotValidator.validateReceive(chipLot);

        chipLot.setReceivedAmount(chipLotDTO.getReceivedAmount());
        chipLot.setArrivalDate(new Date());
        chipLot.setStatus(ChipLotStatusType.RECEIVED);

        updateChipLotItems(chipLot, chipLotDTO);
        List<Chip> chips = updateChipsRegional(chipLot);
        chipHistoryService.lotReceived(chips, chipLot);

        return new ChipLotDTO(chipLotService.save(chipLot));
    }

    public ChipLotDTO cancel(Long chipLotId) {
        ChipLot chipLot = chipLotService.findById(chipLotId);
        chipLotValidator.validateCancel(chipLot);
        List<Chip> chips = new ArrayList<>();
        chipLot.setStatus(ChipLotStatusType.CANCELED);
        chipLot.setCancellationDate(new Date());
        chipLot.getChipLotItems().stream().forEach(chipLotItem -> chipLotItem.setStatus(ChipLotItemStatusType.CANCELED));
        chipLot.getChipLotItems().stream().forEach(chipLotItem -> {
            chipLotItem.setStatus(ChipLotItemStatusType.CANCELED);
            chips.add(chipLotItem.getChip());
        });
        chipHistoryService.lotCanceled(chips, chipLot);
        return new ChipLotDTO(chipLotService.save(chipLot));
    }

    private void updateChipLotItems(ChipLot chipLot, ChipLotDTO chipLotDTO) {
        int i, j;
        boolean found;
        ChipLotItem chipLotItem;
        ChipLotItemDTO chipLotItemDTO;
        for (i = 0; i < chipLot.getChipLotItems().size(); i++) {
            chipLotItem = chipLot.getChipLotItems().get(i);
            found = false;
            chipLotItemDTO = null;
            for (j = chipLotDTO.getChipLotItems().size() - 1; j > -1; j--) {
                chipLotItemDTO = chipLotDTO.getChipLotItems().get(j);
                if (chipLotItem.getId().equals(chipLotItemDTO.getId())) {
                    chipLotDTO.getChipLotItems().remove(j);
                    found = true;
                    break;
                }
            }

            if (found && chipLotItemDTO != null) {
                chipLotItem.setStatus(chipLotItemDTO.getStatus());
            } else {
                chipLotItem.setStatus(ChipLotItemStatusType.NOT_RECEIVED);
            }
        }

        chipLotDTO.getChipLotItems().forEach(chipLotItm -> {
            Chip chip = chipService.findById(chipLotItm.getChip().getId());
            // Não devem ser recebidos chips que não estivavam no estoque da regional que enviou o lote.
            if (chip.getRegionalOffice().equals(chipLot.getRegionalOrigin())) {
                new ChipLotItem(chipLot, chip, ChipLotItemStatusType.RECEIVED_WRONG);
            } else {
                chipLot.setReceivedAmount(chipLot.getReceivedAmount() - 1);
            }
        });
    }

    private List<Chip> updateChipsRegional(ChipLot chipLot) {
        List<Chip> chips = new ArrayList<>();

        chipLot.getChipLotItems().forEach(chipLotItem -> {
            if (!chipLotItem.getStatus().equals(ChipLotItemStatusType.NOT_RECEIVED)) {
                chipLotItem.getChip().setRegionalOffice(chipLot.getRegionalDestination());
                chips.add(chipLotItem.getChip());
            }
        });

        return chipService.saveAll(chips);
    }

    public ChipLotDTO getById(Long chipLotId) {
        return new ChipLotDTO(chipLotService.findById(chipLotId));
    }

    public ChipLotVMO listToReceive(ChipLotVMI vmi) {
        ChipLotVMO vmo = new ChipLotVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("cl.send_date"), Sort.Order.desc("cl.created_date"), Sort.Order.desc("cl.id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage() - 1, tableOption.getItemPerPage(), sort);
        List<Long> regionalIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<ChipLot> page = chipLotService.findListToReceiveByFilters(vmi.getSearchText(), regionalIds, vmi.getLateOnly(), pg);

        vmo.setChipLots(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;

    }

    public ChipLotVMO listSent(ChipLotVMI vmi) {
        ChipLotVMO vmo = new ChipLotVMO();
        TableOptionDTO tableOption = vmi.getTableOption();

        Sort sort = Sort.by(Sort.Order.desc("cl.send_date"), Sort.Order.desc("cl.created_date"), Sort.Order.desc("cl.id"));
        Pageable pg = PageRequest.of(tableOption.getCurrentPage() - 1, tableOption.getItemPerPage(), sort);
        List<Long> regionalOriginIds = sessionUtil.isCurrentUserAdmin() ? null : sessionUtil.getCurrentUserRegionalIds();
        List<Long> regionalDestinationIds = regionalOfficeService.getRegionalIdsFilteredByPermissions(vmi.getRegional());
        PageDTO<ChipLot> page = chipLotService.findSentListByFilters(vmi.getSearchText(), regionalOriginIds, regionalDestinationIds, vmi.getLateOnly(), pg);

        vmo.setChipLots(page.getItems());
        vmo.setTableOption(new TableOptionDTO(page, tableOption));
        return vmo;
    }

    public Integer countWaitingByRegional(Long regionalId) {
        return chipLotService.countWaitingByRegional(regionalId);
    }
}
