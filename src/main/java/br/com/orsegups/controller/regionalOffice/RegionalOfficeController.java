package br.com.orsegups.controller.regionalOffice;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.RegionalOfficeDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.util.List;

@ChipControlController("/api/regionalOffice")
public class RegionalOfficeController {

    @Inject
    private RegionalOfficeFacade regionalOfficeFacade;

    @GetMapping(value = "/list")
    public ResponseEntity<List<RegionalOfficeDTO>> list() {
        return new ResponseEntity<>(regionalOfficeFacade.list(), HttpStatus.OK);
    }

    @GetMapping(value = "/getByAcronym")
    public ResponseEntity<RegionalOfficeDTO> getByAcronym(@RequestParam(name = "acronym") String acronym) {
        return new ResponseEntity<>(regionalOfficeFacade.getByAcronym(acronym), HttpStatus.OK);
    }
}
