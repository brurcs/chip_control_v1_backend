package br.com.orsegups.controller.regionalOffice;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.RegionalOfficeDTO;
import br.com.orsegups.service.RegionalOfficeService;
import br.com.orsegups.util.SessionUtil;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class RegionalOfficeFacade {

    @Inject
    private RegionalOfficeService regionalOfficeService;
    @Inject
    private SessionUtil sessionUtil;

    public List<RegionalOfficeDTO> list() {
        if (sessionUtil.isCurrentUserAdmin()) {
            return regionalOfficeService.findAll().stream().map(RegionalOfficeDTO::new)
                    .sorted(Comparator.comparing(RegionalOfficeDTO::getAcronym)).collect(Collectors.toList());
        } else {
            return sessionUtil.getCurrentUserRegional().stream().map(RegionalOfficeDTO::new)
                    .sorted(Comparator.comparing(RegionalOfficeDTO::getAcronym)).collect(Collectors.toList());
        }
    }

    public RegionalOfficeDTO getByAcronym(String acronym) {
        return new RegionalOfficeDTO(regionalOfficeService.findByAcronym(acronym));
    }

}
