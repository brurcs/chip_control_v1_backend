package br.com.orsegups.controller.dashboard;

import br.com.orsegups.dto.OperatorStatsDTO;
import br.com.orsegups.dto.RegionalStatsDTO;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DashboardVMO {

    private Integer contractedAmount;
    private Integer canceledAmount;
    private Integer customerAmount;
    private Integer stockAmount;
    private Integer technicianAmount;
    private Integer tiStockAmount;
    private Integer inCancellationAmount;
    private Integer trackingAmount;
    private Integer trackingStockAmount;
    private Integer deggyAmount;
    private Integer deggyStockAmount;
    private Integer nextiAmount;
    private Integer nextiStockAmount;
    private Integer primeAmount;
    private Integer primeStockAmount;
    private Integer officeAmount;
    private Integer equipmentAmount;

    private List<RegionalStatsDTO> officeStats;
    private List<RegionalStatsDTO> equipmentStats;
    private List<OperatorStatsDTO> operatorStats;
    private List<RegionalStatsDTO> stockStats;

    public Integer getContractedAmount() {
        return contractedAmount;
    }

    public void setContractedAmount(Integer contractedAmount) {
        this.contractedAmount = contractedAmount;
    }

    public Integer getCanceledAmount() {
        return canceledAmount;
    }

    public void setCanceledAmount(Integer canceledAmount) {
        this.canceledAmount = canceledAmount;
    }

    public Integer getCustomerAmount() {
        return customerAmount;
    }

    public void setCustomerAmount(Integer customerAmount) {
        this.customerAmount = customerAmount;
    }

    public Integer getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(Integer stockAmount) {
        this.stockAmount = stockAmount;
    }

    public Integer getTechnicianAmount() {
        return technicianAmount;
    }

    public void setTechnicianAmount(Integer technicianAmount) {
        this.technicianAmount = technicianAmount;
    }

    public Integer getTiStockAmount() {
        return tiStockAmount;
    }

    public void setTiStockAmount(Integer tiStockAmount) {
        this.tiStockAmount = tiStockAmount;
    }

    public Integer getInCancellationAmount() {
        return inCancellationAmount;
    }

    public void setInCancellationAmount(Integer inCancellationAmount) {
        this.inCancellationAmount = inCancellationAmount;
    }

    public Integer getTrackingAmount() {
        return trackingAmount;
    }

    public void setTrackingAmount(Integer trackingAmount) {
        this.trackingAmount = trackingAmount;
    }

    public Integer getTrackingStockAmount() {
        return trackingStockAmount;
    }

    public void setTrackingStockAmount(Integer trackingStockAmount) {
        this.trackingStockAmount = trackingStockAmount;
    }

    public Integer getDeggyAmount() {
        return deggyAmount;
    }

    public void setDeggyAmount(Integer deggyAmount) {
        this.deggyAmount = deggyAmount;
    }

    public Integer getDeggyStockAmount() {
        return deggyStockAmount;
    }

    public void setDeggyStockAmount(Integer deggyStockAmount) {
        this.deggyStockAmount = deggyStockAmount;
    }

    public Integer getNextiAmount() {
        return nextiAmount;
    }

    public void setNextiAmount(Integer nextiAmount) {
        this.nextiAmount = nextiAmount;
    }

    public Integer getNextiStockAmount() {
        return nextiStockAmount;
    }

    public void setNextiStockAmount(Integer nextiStockAmount) {
        this.nextiStockAmount = nextiStockAmount;
    }

    public Integer getPrimeAmount() {
        return primeAmount;
    }

    public void setPrimeAmount(Integer primeAmount) {
        this.primeAmount = primeAmount;
    }

    public Integer getPrimeStockAmount() {
        return primeStockAmount;
    }

    public void setPrimeStockAmount(Integer primeStockAmount) {
        this.primeStockAmount = primeStockAmount;
    }

    public Integer getOfficeAmount() {
        return officeAmount;
    }

    public void setOfficeAmount(Integer officeAmount) {
        this.officeAmount = officeAmount;
    }

    public Integer getEquipmentAmount() {
        return equipmentAmount;
    }

    public void setEquipmentAmount(Integer equipmentAmount) {
        this.equipmentAmount = equipmentAmount;
    }

    public List<RegionalStatsDTO> getOfficeStats() {
        return officeStats;
    }

    public void setOfficeStats(List<RegionalStatsDTO> officeStats) {
        this.officeStats = officeStats;
    }

    public List<RegionalStatsDTO> getEquipmentStats() {
        return equipmentStats;
    }

    public void setEquipmentStats(List<RegionalStatsDTO> equipmentStats) {
        this.equipmentStats = equipmentStats;
    }

    public List<OperatorStatsDTO> getOperatorStats() {
        return operatorStats;
    }

    public void setOperatorStats(List<OperatorStatsDTO> operatorStats) {
        this.operatorStats = operatorStats;
    }

    public List<RegionalStatsDTO> getStockStats() {
        return stockStats;
    }

    public void setStockStats(List<RegionalStatsDTO> stockStats) {
        this.stockStats = stockStats;
    }
}
