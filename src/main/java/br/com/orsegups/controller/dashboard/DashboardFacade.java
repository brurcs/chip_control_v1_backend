package br.com.orsegups.controller.dashboard;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.service.ChipService;
import br.com.orsegups.service.DashboardService;

import javax.inject.Inject;

@ChipControlFacade
public class DashboardFacade {

    @Inject
    private DashboardService dashboardService;
    @Inject
    private ChipService chipService;

    public DashboardVMO getStats() {
        DashboardVMO vmo = new DashboardVMO();

        vmo.setContractedAmount(chipService.countActiveChips());
        vmo.setCanceledAmount(chipService.countCanceledChips());
        vmo.setCustomerAmount(chipService.countInCustomer());
        vmo.setStockAmount(chipService.countInOfficesStock());
        vmo.setTechnicianAmount(chipService.countInOfficesWithTechnician());
        vmo.setTiStockAmount(chipService.countInStockTI());
        vmo.setInCancellationAmount(chipService.countInCancellationChips());
        vmo.setTrackingAmount(chipService.countIdentifiedInTracking());
        vmo.setTrackingStockAmount(chipService.countInTrackingStock());
        vmo.setDeggyAmount(chipService.countIdentifiedInDeggy());
        vmo.setDeggyStockAmount(chipService.countInDeggyStock());
        vmo.setNextiAmount(chipService.countIdentifiedInNexti());
        vmo.setNextiStockAmount(chipService.countInNextiStock());
        vmo.setPrimeAmount(chipService.countIdentifiedInPrime());
        vmo.setPrimeStockAmount(chipService.countInPrimeStock());

        return vmo;
    }

    public DashboardVMO getStockStats() {
        DashboardVMO vmo = new DashboardVMO();

        vmo.setStockStats(dashboardService.getStockStatsByRegional());

        return vmo;
    }

    public DashboardVMO getOperatorStats() {
        DashboardVMO vmo = new DashboardVMO();

        vmo.setContractedAmount(chipService.countActiveChips());
        vmo.setOperatorStats(dashboardService.getActiveOperatorStats());

        return vmo;
    }

    public DashboardVMO getOfficeStats() {
        DashboardVMO vmo = new DashboardVMO();

        vmo.setOfficeAmount(chipService.countInOffices());
        vmo.setOfficeStats(dashboardService.getOfficesStats());

        return vmo;
    }

    public DashboardVMO getEquipmentStats() {
        DashboardVMO vmo = new DashboardVMO();

        vmo.setEquipmentAmount(chipService.countInEquipments());
        vmo.setEquipmentStats(dashboardService.getEquipmentsStats());

        return vmo;
    }




}
