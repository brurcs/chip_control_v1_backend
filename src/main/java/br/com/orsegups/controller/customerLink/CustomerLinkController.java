package br.com.orsegups.controller.customerLink;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.CustomerLinkDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.inject.Inject;

@ChipControlController("/api/customerLink")
public class CustomerLinkController {

    @Inject
    private CustomerLinkFacade customerLinkFacade;

    @PostMapping(value = "/create")
    public ResponseEntity<CustomerLinkVMO> create(@RequestBody CustomerLinkVMI vmi) {
        return new ResponseEntity<>(customerLinkFacade.create(vmi), HttpStatus.CREATED);
    }

    @PostMapping(value = "/listInstalled")
    public ResponseEntity<CustomerLinkVMO> listInstalled(@RequestBody CustomerLinkVMI vmi) {
        return new ResponseEntity<>(customerLinkFacade.listInstalled(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/cancel")
    public ResponseEntity<CustomerLinkDTO> cancel(@RequestBody CustomerLinkVMI vmi) {
        return new ResponseEntity<>(customerLinkFacade.cancel(vmi), HttpStatus.OK);
    }

}
