package br.com.orsegups.controller.serviceOrder;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.ServiceOrderDTO;
import br.com.orsegups.entity.RegionalOffice;
import br.com.orsegups.externalService.SigmaService;
import br.com.orsegups.util.SessionUtil;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class ServiceOrderFacade {

    @Inject
    private SigmaService sigmaService;
    @Inject
    private SessionUtil sessionUtil;

    public List<ServiceOrderDTO> list() {
        List<ServiceOrderDTO> serviceOrderList = null;
        if (sessionUtil.isCurrentUserRegionalManager()) {
            List<String> regionalAcronyms = sessionUtil.getCurrentUserRegional().stream()
                    .map(RegionalOffice::getAcronym).collect(Collectors.toList());
            serviceOrderList = sigmaService.findServiceOrderByRegional(regionalAcronyms);
        } else if (sessionUtil.isCurrentUserTechnician()) {
            String login = sessionUtil.getCurrentUserLogin();
            serviceOrderList = sigmaService.findServiceOrderByUser(login);
        }

        if (serviceOrderList != null)
            serviceOrderList.sort(Comparator.comparing(ServiceOrderDTO::getId));
        return serviceOrderList;

    }
}
