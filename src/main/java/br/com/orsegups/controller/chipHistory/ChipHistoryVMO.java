package br.com.orsegups.controller.chipHistory;

import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.ChipHistoryDTO;
import br.com.orsegups.entity.Chip;
import br.com.orsegups.entity.ChipHistory;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.stream.Collectors;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChipHistoryVMO {

    private ChipDTO chip;
    private List<ChipHistoryDTO> chipHistories;

    public ChipHistoryVMO() {
        super();
    }

    public ChipHistoryVMO(Chip chip, List<ChipHistory> chipHistories) {
        this.chip = new ChipDTO(chip);
        this.chipHistories = chipHistories.stream().map(ChipHistoryDTO::new).collect(Collectors.toList());
    }

    public ChipDTO getChip() {

        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }

    public List<ChipHistoryDTO> getChipHistories() {
        return chipHistories;
    }

    public void setChipHistories(List<ChipHistoryDTO> chipHistories) {
        this.chipHistories = chipHistories;
    }
}
