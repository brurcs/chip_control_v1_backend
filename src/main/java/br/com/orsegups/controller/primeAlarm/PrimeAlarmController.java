package br.com.orsegups.controller.primeAlarm;

import br.com.orsegups.configuration.annotation.ChipControlController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.inject.Inject;

@ChipControlController("/api/primeAlarm")
public class PrimeAlarmController {

    @Inject
    private PrimeAlarmFacade primeAlarmFacade;

    @PostMapping(value = "/linkCustomerChip")
    public ResponseEntity<Boolean> linkCustomerChip(@RequestBody PrimeAlarmVMI vmi) {
        return new ResponseEntity<>(primeAlarmFacade.linkCustomerChip(vmi), HttpStatus.CREATED);
    }

}