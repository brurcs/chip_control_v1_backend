package br.com.orsegups.controller.primeAlarm;

public class PrimeAlarmVMI {

    private String iccId;
    private String operatorName;
    private Long companyId;
    private String centralId;
    private String centralPartition;
    private String businessName;

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCentralId() {
        return centralId;
    }

    public void setCentralId(String centralId) {
        this.centralId = centralId;
    }

    public String getCentralPartition() {
        return centralPartition;
    }

    public void setCentralPartition(String centralPartition) {
        this.centralPartition = centralPartition;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }
}