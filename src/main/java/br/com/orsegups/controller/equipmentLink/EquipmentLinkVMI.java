package br.com.orsegups.controller.equipmentLink;

import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.RegionalOfficeDTO;
import br.com.orsegups.dto.TableOptionDTO;

public class EquipmentLinkVMI {

    private Long linkId;

    private RegionalOfficeDTO regional;
    private ChipDTO chip;
    private String identifier;

    private String searchText;
    private TableOptionDTO tableOption;
    private String reason;


    public Long getLinkId() {
        return linkId;
    }

    public void setLinkId(Long linkId) {
        this.linkId = linkId;
    }

    public RegionalOfficeDTO getRegional() {
        return regional;
    }

    public void setRegional(RegionalOfficeDTO regional) {
        this.regional = regional;
    }

    public ChipDTO getChip() {
        return chip;
    }

    public void setChip(ChipDTO chip) {
        this.chip = chip;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public TableOptionDTO getTableOption() {
        return tableOption;
    }

    public void setTableOption(TableOptionDTO tableOption) {
        this.tableOption = tableOption;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}