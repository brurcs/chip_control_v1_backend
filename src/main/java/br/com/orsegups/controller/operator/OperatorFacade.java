package br.com.orsegups.controller.operator;

import br.com.orsegups.configuration.annotation.ChipControlFacade;
import br.com.orsegups.dto.OperatorDTO;
import br.com.orsegups.service.OperatorService;

import javax.inject.Inject;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@ChipControlFacade
public class OperatorFacade {

    @Inject
    private OperatorService operatorService;

    public List<OperatorDTO> getAll() {
        return operatorService.findAll().stream().map(OperatorDTO::new)
                .sorted(Comparator.comparing(OperatorDTO::getName)).collect(Collectors.toList());
    }
}
