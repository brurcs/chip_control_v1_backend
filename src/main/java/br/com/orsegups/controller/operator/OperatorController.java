package br.com.orsegups.controller.operator;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.OperatorDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import javax.inject.Inject;
import java.util.List;


@ChipControlController("/api/operator")
public class OperatorController {

    @Inject
    private OperatorFacade operatorFacade;

    @GetMapping(value = "/list")
    public ResponseEntity<List<OperatorDTO>> getAll() {
        return new ResponseEntity<>(operatorFacade.getAll(), HttpStatus.OK);
    }
}
