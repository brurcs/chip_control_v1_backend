package br.com.orsegups.controller.technicianDelivery;

import br.com.orsegups.dto.ChipDTO;
import br.com.orsegups.dto.RegionalOfficeDTO;
import br.com.orsegups.dto.TechnicianDTO;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public class TechnicianDeliveryVMI {

    private List<ChipDTO> chips;
    private TechnicianDTO technician;

    public List<ChipDTO> getChips() {
        return chips;
    }

    public void setChips(List<ChipDTO> chips) {
        this.chips = chips;
    }

    public TechnicianDTO getTechnician() {
        return technician;
    }

    public void setTechnician(TechnicianDTO technician) {
        this.technician = technician;
    }
}
