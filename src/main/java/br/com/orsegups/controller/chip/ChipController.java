package br.com.orsegups.controller.chip;

import br.com.orsegups.configuration.annotation.ChipControlController;
import br.com.orsegups.dto.ChipDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.List;

@ChipControlController("/api/chip")
public class ChipController {

    @Inject
    private ChipFacade chipFacade;

    @PostMapping(value = "/createInBatch")
    public ResponseEntity<List<ChipDTO>> createInBatch(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.createInBatch(vmi), HttpStatus.CREATED);
    }

    @GetMapping(value = "/getByIccId")
    public ResponseEntity<ChipDTO> getByIccId(@RequestParam(name = "iccId") String iccId,
                                              @RequestParam(name = "checkRegional") Boolean checkRegional,
                                              @RequestParam(name = "checkNotInLot") Boolean checkNotInLot) {
        return new ResponseEntity<>(chipFacade.findByIccId(iccId, checkRegional, checkNotInLot), HttpStatus.OK);
    }

    @GetMapping(value = "/findByTechnician")
    public ResponseEntity<List<ChipDTO>> findByTechnician() {
        return new ResponseEntity<>(chipFacade.findByTechnician(), HttpStatus.OK);
    }

    @GetMapping(value = "/findByRegional")
    public ResponseEntity<List<ChipDTO>> findByRegional(@RequestParam(name = "regionalId", required = false) Long regionalId) {
        return new ResponseEntity<>(chipFacade.findByRegional(regionalId), HttpStatus.OK);
    }

    @PostMapping(value = "/findInStock")
    public ResponseEntity<ChipVMO> findInStock(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.findInStock(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/findInTechnician")
    public ResponseEntity<ChipVMO> findInTechnician(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.findInTechnician(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/findInCustomer")
    public ResponseEntity<ChipVMO> findInCustomer(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.findInCustomer(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/findInEquipment")
    public ResponseEntity<ChipVMO> findInEquipment(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.findInEquipment(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/requestCancellation")
    public ResponseEntity<ChipDTO> requestCancellation(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.requestCancellation(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/findToCancel")
    public ResponseEntity<ChipVMO> findToCancel(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.findToCancel(vmi), HttpStatus.OK);
    }

    @GetMapping(value = "/cancel/{id}")
    public ResponseEntity<ChipDTO> cancel(@PathVariable("id") Long id) {
        return new ResponseEntity<>(chipFacade.cancel(id), HttpStatus.OK);
    }

    @GetMapping(value = "/findUsage")
    public ResponseEntity<ChipDTO> findUsage(@RequestParam(name = "iccId") String iccId) {
        return new ResponseEntity<>(chipFacade.findUsage(iccId), HttpStatus.OK);
    }

    @GetMapping(value = "/unlinkTechnician/{id}")
    public ResponseEntity<ChipDTO> unlinkTechnician(@PathVariable("id") Long id) {
        return new ResponseEntity<>(chipFacade.unlinkTechnician(id), HttpStatus.OK);
    }

    @PostMapping(value = "/findCanceled")
    public ResponseEntity<ChipVMO> findCanceled(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.findCanceled(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/undoCancellation")
    public ResponseEntity<ChipDTO> undoCancellation(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.undoCancellation(vmi), HttpStatus.OK);
    }

    @PostMapping("/findChipsFromExcel")
    public ResponseEntity<List<ChipDTO>> findChipsFromExcel(@RequestParam(value="file") MultipartFile multipartFile) {
        return new ResponseEntity<>(chipFacade.findChipsFromExcel(multipartFile), HttpStatus.OK);
    }

    @PostMapping(value = "/cancelInBatch")
    public ResponseEntity<List<ChipDTO>> cancelInBatch(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.cancelInBatch(vmi), HttpStatus.OK);
    }

    @PostMapping(value = "/cancellationInBatch")
    public ResponseEntity<List<ChipDTO>> cancellationInBatch(@RequestBody ChipVMI vmi) {
        return new ResponseEntity<>(chipFacade.cancellationInBatch(vmi), HttpStatus.OK);
    }
}
