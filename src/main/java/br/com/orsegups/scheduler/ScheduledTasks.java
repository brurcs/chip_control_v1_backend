package br.com.orsegups.scheduler;

import br.com.orsegups.scheduler.task.DailyChipCancelTask;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@EnableScheduling
public class ScheduledTasks {

	private static final String TIME_ZONE = "America/Sao_Paulo";

	@Inject
	private DailyChipCancelTask dailyChipCancelTask;

	@Scheduled(cron = "0 0 2 * * *", zone = TIME_ZONE)
	public void dailyChipCancelScheduler(){
		dailyChipCancelTask.run();
	}
	
}
