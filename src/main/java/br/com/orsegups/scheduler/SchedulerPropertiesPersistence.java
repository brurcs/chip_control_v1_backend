package br.com.orsegups.scheduler;

import br.com.orsegups.entity.SchedulerProperties;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
@Transactional
public class SchedulerPropertiesPersistence {

    @PersistenceContext
    private EntityManager entityManager;

    public SchedulerProperties getSchedulerProperties() {
        return entityManager.find(SchedulerProperties.class, 1L);
    }

    public void updateSchedulerProperties(SchedulerProperties schedulerProperties) {
        entityManager.merge(schedulerProperties);
    }

}
