package br.com.orsegups.scheduler.task;

import br.com.orsegups.configuration.locale.Messages;
import br.com.orsegups.dto.CustomerDTO;
import br.com.orsegups.dto.CustomerToCancelDTO;
import br.com.orsegups.entity.CustomerLink;
import br.com.orsegups.entity.SchedulerProperties;
import br.com.orsegups.externalService.SigmaService;
import br.com.orsegups.scheduler.SchedulerPropertiesPersistence;
import br.com.orsegups.service.CustomerLinkService;
import br.com.orsegups.service.utils.CancelUtilService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class DailyChipCancelTask {

    @Inject
    private SigmaService sigmaService;
    @Inject
    private SchedulerPropertiesPersistence schedulerPropertiesPersistence;
    @Inject
    private CustomerLinkService customerLinkService;
    @Inject
    private CancelUtilService cancelUtilService;
    @Inject
    protected Messages messages;

    public void run() {
        SchedulerProperties schedulerProperties = schedulerPropertiesPersistence.getSchedulerProperties();

        CustomerToCancelDTO customerToCancel = sigmaService.getCustomerToCancel(schedulerProperties.getDailyCancelCutoffDate());
        customerToCancel.getCustomers().forEach(this::cancelCustomerChips);

        schedulerProperties.setDailyCancelCutoffDate(customerToCancel.getCutoffDate());
        schedulerPropertiesPersistence.updateSchedulerProperties(schedulerProperties);

    }

    private void cancelCustomerChips(CustomerDTO customer) {
        String cancelReason = messages.get("task.dailyChipCancel.defaultReason");
        List<CustomerLink> customerLinks = customerLinkService.findByRemovalDateIsNullAndCompanyIdAndCentralId(customer.getCompanyId(), customer.getCentralId());
        cancelUtilService.requestChipCancellationByCustomer(customerLinks, cancelReason);
    }

}
